package com.tribeplay.model

data class ToolBarModel(
    var backBtn: Boolean = false,
    var bottomNav: Boolean = false,
    var toolBarShow: Boolean = false,
    var toolBarCancelShow: Boolean = false,
    var toolBarleftTlShow: Boolean = false,
    var toolBarCrossIconShow: Boolean = false,
    var toolBarTtlShow: Boolean = false,
    var toolBarProfileShow: Boolean = false,
    var notiIconShow: Boolean = false,
    var imgGrpUserShow: Boolean = false,
    var leagueListShow: Boolean = false,
    var imgCalenderShow: Boolean = false,
    var locShow: Boolean = false,
    var chatShow: Boolean = false,
    var chatViewDetail: Boolean = false,
    var settingsShow: Boolean = false,
    var profileCalenderShow: Boolean = false,
    var bookMarkShow: Boolean = false,
    var newChatShow: Boolean = false,
    var toolBarTtl: String = "",
    var toolBarLetfTtl: String = ""
)

data class ParamModel(var id: String = "", var isSelect: Boolean = false)


data class GenericModel(
    var message: String = "",
    var status: Boolean = false,
    var otp: Int,
    var bearer_token: String = "",
    var user_id: Int,
    var data: ArrayList<UserDataModel>
)

data class GenericModel1(
    var responseMessage: String,
    var message: String,
    var status: Boolean,
    var data: ArrayList<SportCategoryModel>
)

data class GenericModel2(
    var message: String,
    var status: Boolean,
    var data: ArrayList<GetSelectedSubCateModel>
)

data class SignupModel(
    var email: String,
    var password: String,
    var firebase_tokens: String
)

data class LoginModel(
    var email: String,
    var password: String,
)

data class OTPModel(
    var user_id: String,
    var otp: String,
)

data class ResendOTPModel(
    var email: String = ""
)

data class SetPreferenceModel(
    var user_id: String = "",
    var sport_sub_category_id: String = "",
    var rank_your_self: String = "",
    var start_activity: String = "",
    var communication_with: String = "",
    var following_activity: String = ""
)

data class SelectSportModel(
    var user_id: String = "",
    var sport_category_id: String = "",
    var sport_sub_category_id: String = ""
)

data class GetSelectSportModel(
    var user_id: String,
)

data class GetSelectedSubCateModel(
    var tbl_sport_sub_category_id: Int,
    var sport_category_id: Int,
    var sub_category_name: String = "",
    var status: Int,
    var created_date: String = "",
    var sub_category_image: String = ""
)

data class SelectedSubCatModel(
    var selectedSubCatId: Int = 0,
    var selectedSubCatName: String = "",
)

data class ResetPasswordModel(
    var user_id: String,
    var password: String = "",
    var confirmpassword: String = ""
)

data class UploadImgModel(
    var user_id: String,
    var user_image: String,
)

data class CreateProfileModel(
    var user_id: String,
    var fname: String,
    var lname: String,
    var about_self: String,
    var birthday: String,
    var gender: String,
    var height_type: String,
    var height: String,
    var location: String,
    var right_or_left_handded: String
)

data class SetLocationModel(
    var user_id: String,
    var current_location: String = "",
    var radius: String = "",
    var latitude: String = "",
    var longitude: String = ""
)

data class UserDataModel(
    var tbl_users_id: Int,
    var email: String = "",
    var firebase_tokens: String = "",
    var otp_verify: Int = 0,
    var create_profile: Int = 0,
    var location_check: Int = 0,
    var sport: Int = 0,
    var preferences: Int = 0,
    var fname: String = "",
    var lname: String = "",
    var about_self: String = "",
    var gender: String = "",
    var location: String = "",
    var current_location: String = "",
    var radius: String = "",
    var latitude: String = "",
    var longitude: String = "",
    var jwt_token: String = ""
)


data class SportCategoryModel(
    var isSelect: Boolean = false,
    var tbl_sport_category_id: Int = 0,
    var category_name: String = "",
    var Sport_sub_category: ArrayList<SportSubCategory>
)


data class SportSubCategory(
    var isSelect: Boolean = false,
    var tbl_sport_sub_category_id: Int = 0,
    var sub_category_name: String = ""
)