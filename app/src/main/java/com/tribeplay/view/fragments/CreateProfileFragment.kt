package com.tribeplay.view.fragments

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.RadioButton
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.google.gson.Gson
import com.orhanobut.hawk.Hawk
import com.theartofdev.edmodo.cropper.CropImage
import com.tribeplay.MainActivity
import com.tribeplay.R
import com.tribeplay.databinding.FragmentCreateProfileBinding
import com.tribeplay.model.CreateProfileModel
import com.tribeplay.model.GenericModel
import com.tribeplay.model.ToolBarModel
import com.tribeplay.utils.GlobalConstants
import com.tribeplay.utils.GlobalConstants.FILTER_OBJECT
import com.tribeplay.utils.GlobalConstants.requestKey
import com.tribeplay.utils.SharedPrefClass
import com.tribeplay.utils.setFragmentResultListener
import com.tribeplay.view.base.BaseFragment
import retrofit2.Response
import java.io.File

private const val TAG = "CreateProfileFragment"

class CreateProfileFragment : BaseFragment<FragmentCreateProfileBinding>() {

    private var height1: String = ""
    private var address = ""
    private var imgChecked: Int = 0
    private var imgCount: Int = 0
    private var bitmap: Bitmap? = null
    private var userId: String = ""


    override fun getLayoutId(): Int {
        return R.layout.fragment_create_profile
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.toolBarTtl = getString(R.string.create_profile)
        toolBarModel.backBtn = true
        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (!Hawk.isBuilt()) {
            Hawk.init(requireContext()).build()
        }

        userId = SharedPrefClass().getPrefValue(
            requireContext(), GlobalConstants.USERID
        ).toString()
        if (userId == "0" && userId == "") {
            userId = let { Hawk.get(GlobalConstants.USERID1) ?: "" }
        }
       // Log.d(TAG, "onViewCreated: $userId")

        viewDataBinding?.apply {

            setWheelInch()

            rlCamera.setOnClickListener {
                imgChecked++
                if (imgCount > 3) {
                    imgCount = 0

                } else {
                    imgCount++
                    CropImage.activity().start(requireActivity(), this@CreateProfileFragment)
                }

            }

            etLocationRD.setOnClickListener {
                homeActivity?.displayItAddStack(CreateProfile2Fragment())
            }

            radHInc.setOnClickListener {
                setWheelInch()
            }

            radHCM.setOnClickListener {
                setWheelCM()
            }

            btnCall.setOnClickListener {

                val radioButtonID: Int = rbGrpGender.checkedRadioButtonId
                val radioButton: View = rbGrpGender.findViewById(radioButtonID)
                val idx: Int = rbGrpGender.indexOfChild(radioButton)
                val gender: RadioButton = rbGrpGender.getChildAt(idx) as RadioButton

                val radioButtonID2: Int = radGrpHT.checkedRadioButtonId
                val radioButton2: View = radGrpHT.findViewById(radioButtonID2)
                val idx2: Int = radGrpHT.indexOfChild(radioButton2)
                val height: RadioButton = radGrpHT.getChildAt(idx2) as RadioButton

                val radioButtonID3: Int = radioGroupHand.checkedRadioButtonId
                val radioButton3: View = radioGroupHand.findViewById(radioButtonID3)
                val idx3: Int = radioGroupHand.indexOfChild(radioButton3)
                val hand: RadioButton = radioGroupHand.getChildAt(idx3) as RadioButton

                if (imgChecked > 3 || imgChecked == 3 || imgChecked > 0) {

                    if (isValidate()) {
                        callAPi(
                            CreateProfileModel(
                                userId,
                                etFName.text.toString(),
                                etLName.text.toString(),
                                etTell.text.toString(),
                                etDob.text.toString(),
                                gender.text.toString(),
                                height.text.toString(),
                                height1,
                                address,
                                hand.text.toString()
                            )
                        )

                    }

                } else {
                    showToast("Upload 3 Pictures*")
                }


            }

            etDob.setOnClickListener {
                datePicker(etDob)
            }
        }

        setFragmentResultListener(
            requestKey
        ) { _, result ->
            result.getString(FILTER_OBJECT)?.let { note ->

                address = note
                viewDataBinding?.etLocationRD?.text = note
                val d = Log.d("Dddd222", note)

            }

        }


    }


    private fun callAPi(body: CreateProfileModel) {
        homeActivity?.let {
            ApiCall.createProfileApi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    // Log.d(TAG, "onSuccess: ${Gson().toJson(mResponse)}")
                    if (mResponse.body()?.status == true) {
                        homeActivity?.displayItAddStack(CreateProfile3Fragment())
                    }
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    private fun callAPi2(img: String, userID: String) {
        homeActivity?.let {
            ApiCall.uploadImgApi(it, img, userID, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    if (mResponse.body()?.status == true) {
                        showToast(mResponse.body()?.message)
                        imgChecked++
                        when (imgCount) {
                            1 -> {
                                viewDataBinding?.imgSet1?.setImageBitmap(bitmap)
                            }
                            2 -> {
                                viewDataBinding?.imgSet2?.setImageBitmap(bitmap)
                            }
                            3 -> {
                                viewDataBinding?.imgSet3?.setImageBitmap(bitmap)
                            }
                        }
                    }
                    //  url = mResponse.body()?.result?.url ?: ""

                }

                override fun onError(msg: String) {
                    Log.d("Dddd==", msg.toString())
                }
            })
        }
    }

    var mFile: File? = null
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                val resultUri: Uri = result.uri
                val inputStream = requireContext().contentResolver.openInputStream(resultUri)
                val bitmap1 = BitmapFactory.decodeStream(inputStream)
                bitmap = bitmap1
                val file = File(resultUri.path!!)
                // binding.editTextLogo.setText(file.absolutePath)
                mFile = file

                callAPi2(
                    resultUri.path!!, userId
                )

                //setImageWithUri(mFile, viewDataBinding?.imgAddGallery1)
                //Log.d(TAG, "onActivityResult: $resultUri")
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
                //  Log.d(com.brandappz.dashboard.TAG, "onActivityResult: $error")
            }
        }
    }

    private fun setWheelInch() {

        val list = ArrayList<Int>()

        for (num in 48..84) {
            list.add(num)
        }

        viewDataBinding?.layoutWheel?.apply {
            mainWheelCenter.data = list
            mainWheelCenter.setOnItemSelectedListener { picker, data, position ->
                //showToast("" + data)
                height1 = "" + data
            }

        }
    }

    private fun setWheelCM() {

        val list = ArrayList<Int>()
        for (num in 122..214) {
            list.add(num)
        }

        viewDataBinding?.layoutWheel?.apply {
            mainWheelCenter.data = list
            mainWheelCenter.setOnItemSelectedListener { _, data, position ->
                //showToast("" + data)
                height1 = "" + data
            }

        }
    }

    private fun isValidate(): Boolean {
        viewDataBinding?.apply {
            return when {
                etFName.text.isNullOrEmpty() -> {
                    showToast(getString(R.string.Enter_First_Name_))
                    false
                }
                etDob.text.isNullOrEmpty() -> {
                    showToast(getString(R.string.dob))
                    false
                }
                else -> {
                    true
                }
            }
        }
        return true
    }

}