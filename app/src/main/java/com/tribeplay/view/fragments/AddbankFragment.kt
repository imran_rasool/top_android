package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import com.tribeplay.R
import com.tribeplay.databinding.FragmentAddBankBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.base.BaseFragment


class AddbankFragment(val ttl: String, val btnTxt: String) :
    BaseFragment<FragmentAddBankBinding>() {

    var list = ArrayList<String>()

    override fun getLayoutId(): Int {
        return R.layout.fragment_add_bank
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarTtl = ttl
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true

        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            btnCall.setText(btnTxt)

        }

    }


}