package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tribeplay.R
import com.tribeplay.databinding.*
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.adapters.RecyclerCallback
import com.tribeplay.view.adapters.RecyclerViewGenricAdapter
import com.tribeplay.view.base.BaseFragment


class LeagueAllDetailFragment(val chkFrom: Int) : BaseFragment<FragmentLeagueAllDetailsBinding>() {

    private var rvAdapHeader: RecyclerViewGenricAdapter<String, ItemLeaguesAllDetailsBinding>? =
        null
    private var rvAdapProgress: RecyclerViewGenricAdapter<String, ItemTableRowBinding>? = null

    var list = ArrayList<String>()

    override fun getLayoutId(): Int {
        return R.layout.fragment_league_all_details
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarTtl = "Soccer League"
        toolBarModel.toolBarShow = true
        toolBarModel.leagueListShow = true
        toolBarModel.backBtn = true

        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        list?.clear()
        list.add(getString(R.string.lLeague_detail))
        list.add(getString(R.string.cChat))
        list.add(getString(R.string.pParticipants))
        list.add(getString(R.string.tTeam))
        list.add(getString(R.string.mMatches))
        list.add(getString(R.string.rResults))
        list.add(getString(R.string.pPayments))


        //  viewDataBinding?.apply {
        homeActivity?.viewDataBinding?.recyclerView?.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)


        viewDataBinding?.layoutMatches?.apply {
            if (chkFrom == 1) {
                llLocation.visibility = View.VISIBLE
                ll2.visibility = View.VISIBLE
                llSchedule.visibility = View.GONE
                llSchedule2.visibility = View.GONE
            } else {
                llSchedule.visibility = View.VISIBLE
                llSchedule2.visibility = View.VISIBLE
                llLocation.visibility = View.GONE
                ll2.visibility = View.GONE
            }
            tvScheduleMAtch.setOnClickListener {
                displayItAddStack(ScheduleMatchFragment())
            }

            tvAddResult.setOnClickListener {
                displayItAddStack(AddresultFragment())
            }
            tvScheduleMAtch2.setOnClickListener {
                displayItAddStack(ScheduleMatchFragment())
            }

            tvAddResult2.setOnClickListener {
                displayItAddStack(AddresultFragment())
            }

        }


        viewDataBinding?.apply {

            layoutTeam.apply {
                tvViewDetail.setOnClickListener {
                    displayItAddStack(TeamStatsPlayerFragment())
                }

                tvViewDetail2.setOnClickListener {
                    displayItAddStack(TeamStatsPlayerFragment())
                }
            }
        }



        setAdapterHeader()
        //   setAdapter()


        setAdapter()

        setAdapterChat()
        setAdapterPayment()
        setAdapterResults()
        viewDataBinding?.layoutTeam?.apply {
            setAdapterActive(recyclerViewActive)
            setAdapterActive(recyclerViewActive2)
            setAdapterActive(recyclerViewSubs)
            setAdapterActive(recyclerViewSubs2)
        }


        //  }

    }

    fun setAdapterHeader() {

        var selectPosition = 0
        rvAdapHeader = RecyclerViewGenricAdapter<String, ItemLeaguesAllDetailsBinding>(
            list,
            R.layout.item_leagues_all_details, object :
                RecyclerCallback<ItemLeaguesAllDetailsBinding, String> {
                override fun bindData(
                    binder: ItemLeaguesAllDetailsBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {
                        tvHeader.setText(model)
                        if (position == selectPosition) {
                            tvHeader.setTextColor(setColor(R.color.colorGreen))

                        } else {
                            tvHeader.setTextColor(setColor(R.color.colorBlack2))

                        }

                        linearMain.setOnClickListener {

                            selectPosition = position
                            rvAdapHeader?.notifyDataSetChanged()
                            viewDataBinding?.viewFlipper?.displayedChild = position
                        }


                    }
                }
            })
        homeActivity?.viewDataBinding?.apply {
            recyclerView.adapter = rvAdapHeader
        }
    }


    fun setAdapter() {
        viewDataBinding?.layoutTable?.recyclerView?.layoutManager =
            LinearLayoutManager(context)

        rvAdapProgress = RecyclerViewGenricAdapter<String, ItemTableRowBinding>(
            list,
            R.layout.item_table_row, object :
                RecyclerCallback<ItemTableRowBinding, String> {
                override fun bindData(
                    binder: ItemTableRowBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                        tv1.setText("" + position)

                        if (position == 0) {
                            llHeader.visibility = View.VISIBLE
//
//                            tv1.setText("S No.")
//                            tv2.setText(R.string.player)
//                            tv3.setText(R.string.pPlayer_level)
//                            tv4.setText(R.string.ratings)
                        } else {
                            llHeader.visibility = View.GONE
                        }


                    }
                }
            })
        viewDataBinding?.apply {
            layoutTable.recyclerView.adapter = rvAdapProgress
        }
    }


    fun setAdapterResults() {
        viewDataBinding?.layoutTable1?.recyclerView?.layoutManager =
            LinearLayoutManager(context)

        rvAdapProgress = RecyclerViewGenricAdapter<String, ItemTableRowBinding>(
            list,
            R.layout.item_table_row, object :
                RecyclerCallback<ItemTableRowBinding, String> {
                override fun bindData(
                    binder: ItemTableRowBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                        tv1.setText("" + position)

                        if (position == 0) {
                            llHeader.visibility = View.VISIBLE
//
//                            tv1.setText("S No.")
//                            tv2.setText(R.string.player)
//                            tv3.setText(R.string.pPlayer_level)
//                            tv4.setText(R.string.ratings)
                        } else {
                            llHeader.visibility = View.GONE
                        }


                    }
                }
            })
        viewDataBinding?.apply {
            layoutTable1.recyclerView.adapter = rvAdapProgress
        }
    }


    fun setAdapterChat() {

        viewDataBinding?.layoutChat?.recyclerView?.layoutManager =
            LinearLayoutManager(context)
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        var rvAdapProgress = RecyclerViewGenricAdapter<String, ItemChatMsgBinding>(
            list,
            R.layout.item_chat_msg, object :
                RecyclerCallback<ItemChatMsgBinding, String> {
                override fun bindData(
                    binder: ItemChatMsgBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {


                    }
                }
            })
        viewDataBinding?.apply {
            layoutChat?.recyclerView.adapter = rvAdapProgress
        }
    }


    fun setAdapterPayment() {

        viewDataBinding?.layouPayments?.recyclerView?.layoutManager =
            LinearLayoutManager(context)
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        var rvAdapProgress = RecyclerViewGenricAdapter<String, ItemPaymentBinding>(
            list,
            R.layout.item_payment, object :
                RecyclerCallback<ItemPaymentBinding, String> {
                override fun bindData(
                    binder: ItemPaymentBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {
                        imgDownload.setOnClickListener {
                            displayItAddStack(DownloadRecieptFragment())
                        }

                    }
                }
            })
        viewDataBinding?.apply {
            layouPayments?.recyclerView.adapter = rvAdapProgress
        }
    }


    fun setAdapterActive(recyclerView: RecyclerView) {

        recyclerView?.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        var rvAdapProgress = RecyclerViewGenricAdapter<String, ItemRoundImg3Binding>(
            list,
            R.layout.item_round_img3, object :
                RecyclerCallback<ItemRoundImg3Binding, String> {
                override fun bindData(
                    binder: ItemRoundImg3Binding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {


                    }
                }
            })
        recyclerView.adapter = rvAdapProgress

    }


}