package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import com.tribeplay.R
import com.tribeplay.databinding.FragmentAddCardBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.base.BaseFragment


class AddcardFragment(val ttl: String, val btnTxt: String) :
    BaseFragment<FragmentAddCardBinding>() {

    var list = ArrayList<String>()

    override fun getLayoutId(): Int {
        return R.layout.fragment_add_card
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarTtl = ttl
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true

        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            underlineText(tvAddBackAcc)

            btnCall.setText(btnTxt)

            tvAddBackAcc.setOnClickListener {
                displayItAddStack(AddbankFragment(
                    getString(R.string.add_bank),
                    getString(R.string.done)
                )
                )
            }

            btnCall.setOnClickListener {
                popBack()
            }
        }

    }


}