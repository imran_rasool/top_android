package com.tribeplay.view.fragments

import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.TextView
import androidx.core.view.children
import com.tribeplay.utils.widgets.calenderWidget.model.CalendarDay
import com.tribeplay.utils.widgets.calenderWidget.model.CalendarMonth
import com.tribeplay.utils.widgets.calenderWidget.model.DayOwner

import com.tribeplay.utils.widgets.calenderWidget.utils.next
import com.tribeplay.utils.widgets.calenderWidget.utils.previous
import com.tribeplay.R
import com.tribeplay.databinding.CalendarDayBinding
import com.tribeplay.databinding.CalendarHeaderBinding
import com.tribeplay.databinding.FragmentCalenderBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.utils.widgets.calenderWidget.daysOfWeekFromLocale
import com.tribeplay.utils.widgets.calenderWidget.setTextColorRes
import com.tribeplay.utils.widgets.calenderWidget.ui.DayBinder
import com.tribeplay.utils.widgets.calenderWidget.ui.MonthHeaderFooterBinder
import com.tribeplay.utils.widgets.calenderWidget.ui.ViewContainer
import com.tribeplay.view.base.BaseFragment
import java.time.LocalDate
import java.time.YearMonth
import java.time.format.DateTimeFormatter
import java.time.format.TextStyle
import java.util.*


class CalenderFragment : BaseFragment<FragmentCalenderBinding>() {

    private var binding: FragmentCalenderBinding?=null
    private var selectedDate: LocalDate? = null
    private val monthTitleFormatter = DateTimeFormatter.ofPattern("MMMM")


    override fun getLayoutId(): Int {
        return R.layout.fragment_calender
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.toolBarTtl = getString(R.string.calender)

        toolBarModel.backBtn = true
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // viewDataBinding?.apply {
        binding= viewDataBinding!!
        setCalenderData(binding!!)

        //   }
    }


    fun setCalenderData(binding: FragmentCalenderBinding) {


        val daysOfWeek = daysOfWeekFromLocale()

        var currentMonth = YearMonth.now()
        binding.exFiveCalendar.setup(
            currentMonth.minusMonths(10),
            currentMonth.plusMonths(10),
            daysOfWeek.first()
        )
        binding.exFiveCalendar.scrollToMonth(currentMonth)

        class DayViewContainer(view: View) : ViewContainer(view) {
            lateinit var day: CalendarDay // Will be set when this container is bound.
            val binding = CalendarDayBinding.bind(view)

            init {
                view.setOnClickListener {
                    if (day.owner == DayOwner.THIS_MONTH) {
                        if (selectedDate != day.date) {
                            val oldDate = selectedDate
                            selectedDate = day.date
                            val binding = this@CalenderFragment.binding!!
                            binding.exFiveCalendar.notifyDateChanged(day.date)
                            oldDate?.let { binding.exFiveCalendar.notifyDateChanged(it) }
                            updateAdapterForDate(day.date)
                        }
                    }
                }
            }
        }
        binding!!.exFiveCalendar!!.dayBinder = object : DayBinder<DayViewContainer> {
            override fun create(view: View) = DayViewContainer(view)
            override fun bind(container: DayViewContainer, day: CalendarDay) {
                container.day = day
                val textView = container.binding.exFiveDayText
                val layout = container.binding.exFiveDayLayout
                textView.text = day.date.dayOfMonth.toString()

                val flightTopView = container.binding.exFiveDayFlightTop
                val flightBottomView = container.binding.exFiveDayFlightBottom
                flightTopView.background = null
                flightBottomView.background = null

//                if (day.owner == DayOwner.THIS_MONTH) {
//                    textView.setTextColorRes(R.color.colorGreenLite1)
//                    layout.setBackgroundResource(if (selectedDate == day.date) R.drawable.ic_calender_blue else 0)
//
//                    val flights = flights[day.date]
//                    if (flights != null) {
//                        if (flights.count() == 1) {
//                            flightBottomView.setBackgroundColor(view?.context.getColorCompat(flights[0].color))
//                        } else {
//                            flightTopView.setBackgroundColor(view?.context.getColorCompat(flights[0].color))
//                            flightBottomView.setBackgroundColor(view?.context.getColorCompat(flights[1].color))
//                        }
//                    }
//                } else {
//                    textView.setTextColorRes(R.color.colorLiteGray)
//                    layout.background = null
//                }
            }
        }

        class MonthViewContainer(view: View) : ViewContainer(view) {
            val legendLayout = CalendarHeaderBinding.bind(view).legendLayout.root
        }
        binding.exFiveCalendar.monthHeaderBinder = object :
            MonthHeaderFooterBinder<MonthViewContainer> {
            override fun create(view: View) = MonthViewContainer(view)
            override fun bind(container: MonthViewContainer, month: CalendarMonth) {
                // Setup each header day text if we have not done that already.
                if (container.legendLayout.tag == null) {
                    container.legendLayout.tag = month.yearMonth
                    container.legendLayout.children.map { it as TextView }
                        .forEachIndexed { index, tv ->
                            tv.text =
                                daysOfWeek[index].getDisplayName(TextStyle.SHORT, Locale.ENGLISH)
                                    .toUpperCase(Locale.ENGLISH)
                            tv.setTextColorRes(R.color.colorRed)
                            tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f)
                        }
                    month.yearMonth
                }
            }
        }

        binding.exFiveCalendar.monthScrollListener = { month ->
            val title = "${monthTitleFormatter.format(month.yearMonth)} ${month.yearMonth.year}"
            binding.exFiveMonthYearText.text = title

            selectedDate?.let {
                // Clear selection if we scroll to a new month.
                selectedDate = null
                binding.exFiveCalendar.notifyDateChanged(it)
                updateAdapterForDate(null)
            }
        }

        binding.exFiveNextMonthImage.setOnClickListener {
            binding.exFiveCalendar.findFirstVisibleMonth()?.let {
                binding.exFiveCalendar.smoothScrollToMonth(it.yearMonth.next)
            }
        }

        binding.exFivePreviousMonthImage.setOnClickListener {
            binding.exFiveCalendar.findFirstVisibleMonth()?.let {
                binding.exFiveCalendar.smoothScrollToMonth(it.yearMonth.previous)
            }
        }
    }

    private fun updateAdapterForDate(date: LocalDate?) {
        Log.d("dddd","dsdsdsdsds")
//        flightsAdapter.flights.clear()
//        flightsAdapter.flights.addAll(flights[date].orEmpty())
//        flightsAdapter.notifyDataSetChanged()
    }

}



