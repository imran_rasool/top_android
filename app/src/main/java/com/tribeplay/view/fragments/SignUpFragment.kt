package com.tribeplay.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.MotionEvent
import android.view.View
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.google.gson.Gson
import com.orhanobut.hawk.Hawk
import com.tribeplay.R
import com.tribeplay.databinding.FragmentSignupBinding
import com.tribeplay.model.GenericModel
import com.tribeplay.model.SignupModel
import com.tribeplay.model.ToolBarModel
import com.tribeplay.utils.GlobalConstants
import com.tribeplay.utils.SharedPrefClass
import com.tribeplay.utils.UtilsFunctions
import com.tribeplay.view.base.BaseFragment
import retrofit2.Response


private const val TAG = "SignUpFragment"

class SignUpFragment : BaseFragment<FragmentSignupBinding>() {

    var isPasswordVisibe1 = false
    var isPasswordVisibe2 = false


    override fun getLayoutId(): Int {
        return R.layout.fragment_signup
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false

        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (!Hawk.isBuilt()) {
            Hawk.init(requireContext()).build()
        }

        viewDataBinding?.apply {
            tvSignIn.setOnClickListener {
                homeActivity?.popBack()
            }

            etSetPass.setOnTouchListener(View.OnTouchListener { v, event ->
                val DRAWABLE_RIGHT = 2
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX >= etSetPass.right - etSetPass.compoundDrawables[DRAWABLE_RIGHT].bounds.width()
                    ) {
                        isPasswordVisibe1 = !isPasswordVisibe1

                        if (isPasswordVisibe1) {
                            etSetPass.transformationMethod =
                                HideReturnsTransformationMethod.getInstance();
                            etSetPass.setCompoundDrawablesWithIntrinsicBounds(
                                0,
                                0,
                                R.drawable.ic_icon_visibility,
                                0
                            );
                        } else {
                            etSetPass.transformationMethod =
                                PasswordTransformationMethod.getInstance();
                            etSetPass.setCompoundDrawablesWithIntrinsicBounds(
                                0,
                                0,
                                R.drawable.ic_resource_private,
                                0
                            );
                        }

                        return@OnTouchListener true
                    }
                }
                false
            })

            etConfirmPass.setOnTouchListener(View.OnTouchListener { v, event ->
                val DRAWABLE_RIGHT = 2
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX >= etConfirmPass.right - etConfirmPass.compoundDrawables[DRAWABLE_RIGHT].bounds.width()
                    ) {
                        isPasswordVisibe2 = !isPasswordVisibe2

                        if (isPasswordVisibe2) {
                            etConfirmPass.transformationMethod =
                                HideReturnsTransformationMethod.getInstance();
                            etConfirmPass.setCompoundDrawablesWithIntrinsicBounds(
                                0,
                                0,
                                R.drawable.ic_icon_visibility,
                                0
                            );
                        } else {
                            etConfirmPass.transformationMethod =
                                PasswordTransformationMethod.getInstance();
                            etConfirmPass.setCompoundDrawablesWithIntrinsicBounds(
                                0,
                                0,
                                R.drawable.ic_resource_private,
                                0
                            );
                        }

                        return@OnTouchListener true
                    }
                }
                false
            })

            underlineText(tvSignIn)

            btnCall.setOnClickListener {

                if (isValidate()) {
                    callAPi(
                        SignupModel(
                            etEmail.text.toString(),
                            etSetPass.text.toString(),
                            ""
                        )
                    )

                }
            }
        }
    }

    private fun callAPi(body: SignupModel) {
        homeActivity?.let {
            ApiCall.signupApi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    //Log.d(TAG, "onSuccess: ${Gson().toJson(mResponse)}")
                    if (mResponse.body()?.status == true) {
                        SharedPrefClass().putObject(
                            it,
                            GlobalConstants.ACCESS_TOKEN,
                            mResponse.body()?.bearer_token
                        )
                        SharedPrefClass().putObject(
                            it, GlobalConstants.USERID, mResponse.body()?.user_id
                        )

                        val userid: String = let { mResponse.body()?.user_id.toString() }
                        Hawk.put(GlobalConstants.USERID1, userid)

                        val fromOTP: Int = let { mResponse.body()?.otp ?: 0 }
                        viewDataBinding?.apply {
                            val email = etEmail.text.toString()
                            homeActivity?.displayItAddStack(OtpverificationFragment(0, email))

                        }


                    }

                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    private fun isValidate(): Boolean {
        viewDataBinding?.apply {
            if (etEmail.text.isNullOrEmpty()) {
                showToast(getString(R.string.valid_enter_email))
                return false
            } else if (!UtilsFunctions.isValidEmail(etEmail.text.toString())) {
                showToast(getString(R.string.valid_email))
                return false
            } else if (etSetPass.text.isNullOrEmpty()) {
                showToast(getString(R.string.enter_password))
                return false
            } else if (etSetPass.text.toString().length < 7) {
                showToast(getString(R.string.valid_pass))
                return false
            } else if (etConfirmPass.text.isNullOrEmpty()) {
                showToast(getString(R.string.enter_confirm_password))
                return false
            } else if (!etSetPass.text.toString()
                    .equals(etConfirmPass.text.toString(), ignoreCase = false)
            ) {
                showToast(getString(R.string.pass_does_not_match))
                return false
            } else {
                return true
            }
        }
        return true
    }


}