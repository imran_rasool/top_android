package com.tribeplay.view.fragments

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.annotation.Nullable
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.tribeplay.R
import com.tribeplay.view.adapters.BottomFragmentCallback

class ActionBottomDialogFragment<T, VM : ViewDataBinding>(
    val layoutId: Int,
    val bindingInterface: BottomFragmentCallback<VM, T>
) : BottomSheetDialogFragment() {

    val TAG = "ActionBottomDialog"
    var binding: VM? = null

    @Nullable
    override fun onCreateView(
        inflater: LayoutInflater, @Nullable container: ViewGroup?,
        @Nullable savedInstanceState: Bundle?
    ): View? {
        getDialog()?.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog()?.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MyDialog);
        binding =
            DataBindingUtil.inflate(inflater, layoutId, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        if (binding == null)
            binding = DataBindingUtil.bind(view)

        if (binding != null)
            bindingInterface.bindData(binding!!)


    }


}