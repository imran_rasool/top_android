package com.tribeplay.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RadioButton
import androidx.core.content.ContextCompat
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.appsums.utils.widgets.monthyearpicker.YearMonthPickerDialog
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.google.gson.Gson
import com.orhanobut.hawk.Hawk
import com.tribeplay.R
import com.tribeplay.databinding.FragmentCreateProfile4Binding
import com.tribeplay.databinding.ItemImgTvBoxBinding
import com.tribeplay.model.*
import com.tribeplay.utils.GlobalConstants
import com.tribeplay.utils.SharedPrefClass
import com.tribeplay.utils.widgets.customseekbar.widget.IndicatorSeekBar
import com.tribeplay.utils.widgets.customseekbar.widget.OnSeekChangeListener
import com.tribeplay.utils.widgets.customseekbar.widget.SeekParams
import com.tribeplay.view.adapters.RecyclerCallback
import com.tribeplay.view.adapters.RecyclerViewGenricAdapter
import com.tribeplay.view.base.BaseFragment
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

private const val TAG = "CreateProfile4Fragment"

class CreateProfile4Fragment :
    BaseFragment<FragmentCreateProfile4Binding>() {

    private var rvAdapter: RecyclerViewGenricAdapter<GetSelectedSubCateModel, ItemImgTvBoxBinding>? =
        null
    private val subCateList = ArrayList<GetSelectedSubCateModel>()
    private var catID = ""
    private var rank = "0"
    private var yearSelected = 0
    private var monthSelected = 0
    private var isNextFinished: Boolean = false
    private var clickCount = 0
    private var followingAct: String = ""
    private var userId: String = ""


    override fun getLayoutId(): Int {
        return R.layout.fragment_create_profile4
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.set_preference)

        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (!Hawk.isBuilt()) {
            Hawk.init(requireContext()).build()
        }

        userId = SharedPrefClass().getPrefValue(requireContext(), GlobalConstants.USERID).toString()
        if (userId == "0" && userId == "") {
            userId = let { Hawk.get(GlobalConstants.USERID1) ?: "" }
        }

        viewDataBinding?.apply {

            rankSB.onSeekChangeListener = object : OnSeekChangeListener {
                override fun onSeeking(seekParams: SeekParams) {
                    Log.i(TAG, seekParams.progressFloat.toString())
                    //rank = seekParams.progressFloat.toString()
                    rank = seekParams.progress.toString()
                    Log.d(TAG, "onSeeking: $rank")
                }

                override fun onStartTrackingTouch(seekBar: IndicatorSeekBar) {
                }

                override fun onStopTrackingTouch(seekBar: IndicatorSeekBar) {

                }

            }

            etDob.setOnClickListener {
                setMonthYear()
            }

            coachRB.setOnClickListener {
                coachRB.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.white
                    )
                )
                followingAct += getString(R.string.cCoach)
                coachRB.setBackgroundResource(R.drawable.round_solid_green_3_btn)

            }

            volunteerRB.setOnClickListener {
                volunteerRB.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.white
                    )
                )
                followingAct += getString(R.string.vVolunteer)
                volunteerRB.setBackgroundResource(R.drawable.round_solid_green_3_btn)

            }

            managerRB.setOnClickListener {
                managerRB.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.white
                    )
                )
                followingAct += getString(R.string.mManager)
                managerRB.setBackgroundResource(R.drawable.round_solid_green_3_btn)

            }

            btnCall.setOnClickListener {

                val radioButtonID1: Int = radioGroupComm1.checkedRadioButtonId
                val radioButton1: View = radioGroupComm1.findViewById(radioButtonID1)
                val idx1: Int = radioGroupComm1.indexOfChild(radioButton1)
                val communication: RadioButton = radioGroupComm1.getChildAt(idx1) as RadioButton

                if (isValidate()) {
                    callAPi(
                        SetPreferenceModel(
                            userId,
                            catID,
                            rank,
                            etDob.text.toString(),
                            communication.text.toString(),
                            followingAct
                        )
                    )
                }

            }

        }


        callApi3(
            GetSelectSportModel(
                userId
            )
        )

    }


    private fun callApi3(body: GetSelectSportModel) {
        homeActivity?.let {
            ApiCall.getSelectCategoryApi(it, body, object : ResponseListener<GenericModel2> {
                override fun onSuccess(mResponse: Response<GenericModel2>) {
                    subCateList.clear()
                    mResponse.body()?.data?.let { it1 -> subCateList.addAll(it1) }

                    viewDataBinding?.apply {
                        val layoutManager1 = FlexboxLayoutManager(context)
                        layoutManager1.flexDirection = FlexDirection.ROW
                        layoutManager1.justifyContent = JustifyContent.CENTER
                        recyclerView.layoutManager = layoutManager1
                        setAdapter()
                        rvAdapter?.notifyDataSetChanged()

                    }


                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    private fun callAPi(body: SetPreferenceModel) {
        homeActivity?.let {
            ApiCall.setPreferenceApi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    //  Log.d(TAG, "onSuccess: ${Gson().toJson(mResponse.body()?.message)}")
                    if (mResponse.body()?.status == true) {
                        clickCount++

                        viewDataBinding?.apply {
                            etDob.setText("")
                            followingAct = ""
                            coachRB.setTextColor(
                                ContextCompat.getColor(
                                    requireContext(),
                                    R.color.colorBlack3
                                )
                            )
                            volunteerRB.setTextColor(
                                ContextCompat.getColor(
                                    requireContext(),
                                    R.color.colorBlack3
                                )
                            )
                            managerRB.setTextColor(
                                ContextCompat.getColor(
                                    requireContext(), R.color.colorBlack3
                                )
                            )

                            coachRB.setBackgroundResource(R.drawable.edit_bg)
                            volunteerRB.setBackgroundResource(R.drawable.edit_bg)
                            managerRB.setBackgroundResource(R.drawable.edit_bg)


                        }
                        viewDataBinding?.apply {
                            if (clickCount == subCateList.size - 1)
                                btnCall.text = getString(R.string.finished)
                        }

                        if (clickCount == subCateList.size) {
                            isNextFinished = true
                        }
                        showToast(mResponse.body()?.message)
                        //homeActivity?.displayItAddStack(HomeFragment())
                        if (isNextFinished) {
                            homeActivity?.viewDataBinding?.bottomNavigationView?.selectedItemId =
                                R.id.action_grp
                        }

                    }
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    private var mPosition: Int = -1
    private fun setAdapter() {

        rvAdapter = RecyclerViewGenricAdapter<GetSelectedSubCateModel, ItemImgTvBoxBinding>(
            subCateList,
            R.layout.item_img_tv_box, object :
                RecyclerCallback<ItemImgTvBoxBinding, GetSelectedSubCateModel> {
                override fun bindData(
                    binder: ItemImgTvBoxBinding,
                    model: GetSelectedSubCateModel,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                        txtName.text = model.sub_category_name
                        val imgURL = "http://api-dev.tribesofplay.com" + model.sub_category_image
                        setImageWithUrl(imgURL, imgIcon)

                        if (mPosition == position) {

                            llItemTop.background = ContextCompat.getDrawable(
                                requireContext(),
                                R.drawable.round_solid_green_3_btn
                            )
                            txtName.setTextColor(
                                ContextCompat.getColor(
                                    requireContext(),
                                    R.color.white
                                )
                            )

                        } else {
                            llItemTop.background = ContextCompat.getDrawable(
                                requireContext(),
                                R.drawable.edit_bg
                            )
                            txtName.setTextColor(
                                ContextCompat.getColor(
                                    requireContext(),
                                    R.color.black
                                )
                            )
                        }

                        llItemTop.setOnClickListener {
                            mPosition = position
                            catID = "" + model.tbl_sport_sub_category_id
                            rvAdapter?.notifyDataSetChanged()
                        }

                    }
                }
            })

        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapter
        }
    }

    private fun setMonthYear() {
        val calendar1 = Calendar.getInstance()
        val yearMonthPickerDialog = YearMonthPickerDialog(
            requireContext(),
            object : YearMonthPickerDialog.OnDateSetListener {
                @SuppressLint("SimpleDateFormat")
                override fun onYearMonthSet(year: Int, month: Int) {
                    val calendar = Calendar.getInstance()
                    calendar[Calendar.YEAR] = year
                    calendar[Calendar.MONTH] = month
                    yearSelected = year
                    monthSelected = month + 1
                    // val dateFormat = SimpleDateFormat("MMM/yyyy")
                    val dateFormat = SimpleDateFormat("yyyy")

                    val currentTime = Calendar.getInstance().time
                    if (currentTime >= calendar.time) {
                        viewDataBinding?.etDob?.setText(dateFormat.format(calendar.time))

                    } else {
                        showToast("Choose Previous Year")
                    }


                }

            },
            calendar1
        )

        yearMonthPickerDialog.show()

    }

    private fun isValidate(): Boolean {
        viewDataBinding?.apply {
            if (etDob.text.isNullOrEmpty()) {
                showToast("Enter Year")
                return false
            }
        }
        return true
    }


}