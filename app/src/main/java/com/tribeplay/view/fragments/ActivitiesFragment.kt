package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayout
import com.tribeplay.R
import com.tribeplay.databinding.FragmentActivitiesBinding
import com.tribeplay.databinding.ItemActivitiesBinding
import com.tribeplay.databinding.ItemMarkInterestBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.adapters.RecyclerCallback
import com.tribeplay.view.adapters.RecyclerViewGenricAdapter
import com.tribeplay.view.base.BaseFragment


class ActivitiesFragment : BaseFragment<FragmentActivitiesBinding>() {


    private var rvAdapProgress: RecyclerViewGenricAdapter<String, ItemActivitiesBinding>? = null
    private var selectPosition: Int = 0

    override fun getLayoutId(): Int {
        return R.layout.fragment_activities
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarTtl = getString(R.string.aActivities)
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = false
        toolBarModel.bottomNav = true
        toolBarModel.notiIconShow = true
        toolBarModel.toolBarProfileShow = true
        toolBarModel.bookMarkShow = true
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            recyclerView.layoutManager =
                LinearLayoutManager(context)
            setAdapter(0)


            if (tabLayout.tabCount <= 0) {
                tabLayout.addTab(tabLayout.newTab().setText(R.string.mMy_activities))
                tabLayout.addTab(tabLayout.newTab().setText(R.string.mAvailable_activities))

            }

            llUpcoming.setOnClickListener {
                llUpcoming.setBackgroundResource(R.drawable.round_solid_blue_lite_btn)
                tvupcoming.setTextColor(setColor(R.color.colorBtn))
                tvPast.setTextColor(setColor(R.color.black))
                llPast.setBackgroundResource(R.drawable.round_solid_gray_lite_btn)
                setAdapter(0)
            }

            tvSchedule.setOnClickListener {
                homeActivity?.displayItAddStack(SetupActivitiesFragment())
            }

            tvViewReq.setOnClickListener {
                homeActivity?.displayItAddStack(RequestFragment())
            }

            llPast.setOnClickListener {
                llPast.setBackgroundResource(R.drawable.round_solid_blue_lite_btn)
                llUpcoming.setBackgroundResource(R.drawable.round_solid_gray_lite_btn)
                tvPast.setTextColor(setColor(R.color.colorBtn))
                tvupcoming.setTextColor(setColor(R.color.black))

                setAdapter(1)
            }


            tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab?) {
                    selectPosition = tab?.position ?: 0
                    if (selectPosition == 1) {
                        llMainSport.visibility = View.GONE
                        setAdapter()
                    } else {
                        llMainSport.visibility = View.VISIBLE
                        llUpcoming.setBackgroundResource(R.drawable.round_solid_blue_lite_btn)
                        tvupcoming.setTextColor(setColor(R.color.colorBtn))
                        tvPast.setTextColor(setColor(R.color.black))
                        llPast.setBackgroundResource(R.drawable.round_solid_gray_lite_btn)
                        setAdapter(0)
                    }

                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {

                }

                override fun onTabReselected(tab: TabLayout.Tab?) {

                }

            })


        }


    }


    fun setAdapter(i: Int) {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        rvAdapProgress = RecyclerViewGenricAdapter<String, ItemActivitiesBinding>(
            list,
            R.layout.item_activities, object :
                RecyclerCallback<ItemActivitiesBinding, String> {
                override fun bindData(
                    binder: ItemActivitiesBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                        if (i == 0) {
                            llEdit.visibility = View.VISIBLE
                        } else {
                            llEdit.visibility = View.GONE
                        }

                        linearMain.setOnClickListener {
                            homeActivity?.displayItAddStack(ActivityDetailFragment())
                        }

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


    fun setAdapter() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemMarkInterestBinding>(
            list,
            R.layout.item_mark_interest, object :
                RecyclerCallback<ItemMarkInterestBinding, String> {
                override fun bindData(
                    binder: ItemMarkInterestBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {
                        btnCall.visibility = View.GONE
                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }

}