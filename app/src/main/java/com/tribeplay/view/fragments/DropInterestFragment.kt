package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.tribeplay.R
import com.tribeplay.databinding.FragmentDropInterestBinding
import com.tribeplay.databinding.ItemImgTvBoxBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.adapters.RecyclerCallback
import com.tribeplay.view.adapters.RecyclerViewGenricAdapter
import com.tribeplay.view.base.BaseFragment


class DropInterestFragment(var ttl: String) : BaseFragment<FragmentDropInterestBinding>() {


    override fun getLayoutId(): Int {
        return R.layout.fragment_drop_interest
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.toolBarTtl = ttl
        toolBarModel.backBtn = true



        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            recyclerView.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            setAdapter()

            if (ttl == getString(R.string.create_group)) {
                btnCall.setText(R.string.next)
                btnCall.setOnClickListener {
                    homeActivity?.displayItAddStack(PickPlayerFragment())
                }
            } else {
                btnCall.setOnClickListener {
                    popBack()
                }
            }

        }

    }


    fun setAdapter() {
        val list = ArrayList<String>()
        list.add("Cricket")
        list.add("Tennis")
        list.add("Racket")
        list.add("BasketBall")


        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemImgTvBoxBinding>(
            list,
            R.layout.item_img_tv_box, object :
                RecyclerCallback<ItemImgTvBoxBinding, String> {
                override fun bindData(
                    binder: ItemImgTvBoxBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {
                        txtName.text = model
                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }

}