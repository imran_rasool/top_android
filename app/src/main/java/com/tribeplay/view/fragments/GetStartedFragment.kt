package com.tribeplay.view.fragments

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import com.tribeplay.R
import com.tribeplay.databinding.FragmentGetstartBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.base.BaseFragment


class GetStartedFragment : BaseFragment<FragmentGetstartBinding>() {


    override fun getLayoutId(): Int {
        return R.layout.fragment_getstart
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //setStatusBarColor()
//        StatusBarUtil.setColor(thiss,R.color.white)
        //  StatusBarUtil.setTranslucent(requireActivity())
        viewDataBinding?.apply {

            btnGetStart.setOnClickListener {
                homeActivity?.displayItNoStack(NextFragment())
            }
        }

    }


    fun setStatusBarColor() {
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true)
        }
        if (Build.VERSION.SDK_INT >= 19) {
            activity?.window!!.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false)
            activity?.window!!.statusBarColor = Color.TRANSPARENT
        }

    }

    private fun setWindowFlag(bits: Int, on: Boolean) {
        val win = requireActivity().window
        val winParams = win.attributes
        if (on) {
            winParams.flags = winParams.flags or bits
        } else {
            winParams.flags = winParams.flags and bits.inv()
        }
        win.attributes = winParams
    }


}