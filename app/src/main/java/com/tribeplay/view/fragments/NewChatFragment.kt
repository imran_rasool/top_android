package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.tribeplay.R
import com.tribeplay.databinding.FragmentNewChatsBinding
import com.tribeplay.databinding.ItemNewChatBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.adapters.RecyclerCallback
import com.tribeplay.view.adapters.RecyclerViewGenricAdapter
import com.tribeplay.view.base.BaseFragment


class NewChatFragment : BaseFragment<FragmentNewChatsBinding>() {


    override fun getLayoutId(): Int {
        return R.layout.fragment_new_chats
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.toolBarTtl = getString(R.string.new_chat)
        toolBarModel.backBtn = true
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            recyclerView.layoutManager =
                LinearLayoutManager(context)
            setAdapter()
        }

    }


    fun setAdapter() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemNewChatBinding>(
            list,
            R.layout.item_new_chat, object :
                RecyclerCallback<ItemNewChatBinding, String> {
                override fun bindData(
                    binder: ItemNewChatBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


}