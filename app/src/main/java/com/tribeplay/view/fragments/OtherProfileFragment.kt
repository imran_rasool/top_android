package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayout
import com.tribeplay.R
import com.tribeplay.databinding.FragmentOtherProfileBinding
import com.tribeplay.databinding.FragmentProfileBinding
import com.tribeplay.databinding.ItemReviewBinding
import com.tribeplay.databinding.ItemSportsBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.adapters.RecyclerCallback
import com.tribeplay.view.adapters.RecyclerViewGenricAdapter
import com.tribeplay.view.base.BaseFragment


class OtherProfileFragment : BaseFragment<FragmentOtherProfileBinding>() {


    private var selectPosition: Int = 0

    override fun getLayoutId(): Int {
        return R.layout.fragment_other_profile
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.toolBarTtl = getString(R.string.profile)
        toolBarModel.backBtn = true
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            recyclerView.layoutManager = LinearLayoutManager(context)
            recyclerViewReview.layoutManager = LinearLayoutManager(context)
            setAdapter()
            setReviewAdapter()

            if (tabLayout.tabCount <= 0) {
                tabLayout.addTab(tabLayout.newTab().setText(R.string.aAbout))
                tabLayout.addTab(tabLayout.newTab().setText(R.string.sSports))
                tabLayout.addTab(tabLayout.newTab().setText(R.string.rReviews))
                tabLayout.addTab(tabLayout.newTab().setText(R.string.mMatches))
            }

            tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab?) {
                    selectPosition = tab?.position ?: 0

                    when (selectPosition) {
                        2 -> {
                            recyclerViewReview.visibility = View.VISIBLE
                            recyclerView.visibility = View.GONE
                            scrollAbout.visibility = View.GONE
                        }
                        1 -> {
                            recyclerViewReview.visibility = View.GONE
                            recyclerView.visibility = View.VISIBLE
                            scrollAbout.visibility = View.GONE

                        }
                        else -> {
                            recyclerViewReview.visibility = View.GONE
                            recyclerView.visibility = View.GONE
                            scrollAbout.visibility = View.VISIBLE

                        }
                    }

                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {

                }

                override fun onTabReselected(tab: TabLayout.Tab?) {

                }

            })

        }


    }


    fun setAdapter() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemSportsBinding>(
            list,
            R.layout.item_sports, object :
                RecyclerCallback<ItemSportsBinding, String> {
                override fun bindData(
                    binder: ItemSportsBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


    fun setReviewAdapter() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemReviewBinding>(
            list,
            R.layout.item_review, object :
                RecyclerCallback<ItemReviewBinding, String> {
                override fun bindData(
                    binder: ItemReviewBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerViewReview.adapter = rvAdapProgress
        }
    }


}