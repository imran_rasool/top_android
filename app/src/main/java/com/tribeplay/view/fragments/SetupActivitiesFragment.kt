package com.tribeplay.view.fragments

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.app.SharedElementCallback
import androidx.recyclerview.widget.LinearLayoutManager
import com.tribeplay.R
import com.tribeplay.databinding.FragmentSetupActivitiesBinding
import com.tribeplay.databinding.ItemActivitiesBinding
import com.tribeplay.databinding.ItemImgTvBoxBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.adapters.RecyclerCallback
import com.tribeplay.view.adapters.RecyclerViewGenricAdapter
import com.tribeplay.view.base.BaseFragment


class SetupActivitiesFragment : BaseFragment<FragmentSetupActivitiesBinding>() {


    private var rvAdapProgress: RecyclerViewGenricAdapter<String, ItemActivitiesBinding>? = null
    private var selectPosition: Int = 0

    override fun getLayoutId(): Int {
        return R.layout.fragment_setup_activities
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.toolBarleftTlShow = true
        toolBarModel.toolBarCrossIconShow = true
        toolBarModel.toolBarLetfTtl = getString(R.string.sSet_up_activities)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


//        homeActivity?.viewDataBinding?.imgCrossbtn?.setOnClickListener {
//            viewDataBinding?.viewFlipper?.showPrevious()
//        }

        viewDataBinding?.apply {
            recyclerView.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            setAdapter()
            viewFlipper.displayedChild = 3
            btnCall.setOnClickListener {
                Log.d("Dddd", "" + viewFlipper.displayedChild)
                when (viewFlipper.displayedChild) {
                    2 -> {
                        homeActivity?.popBack()
                    }
                    0 -> {
                        setHighliteView(view2, view3, view1)
                        viewFlipper.showNext()
                    }
                    1 -> {
                        setHighliteView(view3, view2, view1)
                        viewFlipper.showNext()
                    }
                }


            }

        }


    }


    fun setHighliteView(view1: View, view2: View, view3: View) {
        view1.setBackgroundResource(R.drawable.round_solid_blue_btn)
        view2.setBackgroundResource(R.drawable.round_solid_dark_gray_btn)
        view3.setBackgroundResource(R.drawable.round_solid_dark_gray_btn)

    }


    fun setAdapter() {
        val list = ArrayList<String>()
        list.add("Cricket")
        list.add("Tennis")
        list.add("Racket")
        list.add("BasketBall")


        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemImgTvBoxBinding>(
            list,
            R.layout.item_img_tv_box, object :
                RecyclerCallback<ItemImgTvBoxBinding, String> {
                override fun bindData(
                    binder: ItemImgTvBoxBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {
                        txtName.text = model
                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


}