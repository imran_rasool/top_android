package com.tribeplay.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.mukesh.OnOtpCompletionListener
import com.orhanobut.hawk.Hawk
import com.tribeplay.R
import com.tribeplay.databinding.FragmentOtpVerificationBinding
import com.tribeplay.model.GenericModel
import com.tribeplay.model.OTPModel
import com.tribeplay.model.ResendOTPModel
import com.tribeplay.model.ToolBarModel
import com.tribeplay.utils.GlobalConstants
import com.tribeplay.utils.SharedPrefClass
import com.tribeplay.view.base.BaseFragment
import retrofit2.Response


class OtpverificationFragment(val from: Int, val email: String) :
    BaseFragment<FragmentOtpVerificationBinding>() {

    var otp = ""
    private val millisInFuture: Long = 60000 //120000
    private val countDownInterval: Long = 1000
    private var expireTime: Int = 0
    private var isPaused = false
    private var isCancelled = false
    private var resumeFromMillis: Long = 0

    override fun getLayoutId(): Int {
        return R.layout.fragment_otp_verification
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (!Hawk.isBuilt()) {
            Hawk.init(requireContext()).build()
        }

        timer(millisInFuture, countDownInterval).start()

        viewDataBinding?.apply {

            tvEmail1.text = email
            otpView.setOtpCompletionListener {
                otp = it
            }

            btnVerify.setOnClickListener {

                if (expireTime != 0) {

                    if (otp.length == 4) {
                        var userId = SharedPrefClass().getPrefValue(
                            requireContext(),
                            GlobalConstants.USERID
                        ).toString()

                        if (userId == "0" && userId == "") {
                            userId = let { Hawk.get(GlobalConstants.USERID1) ?: "" }
                        }
                        callAPi(
                            OTPModel(
                                userId, otp
                            )
                        )
                    } else {
                        showToast("Please enter otp")
                    }

                } else {
                    showToast("Your OTP has expired")
                }
            }

            tvResendOTP.setOnClickListener {
                callResendAPi(
                    ResendOTPModel(
                        SharedPrefClass().getPrefValue(
                            requireContext(),
                            GlobalConstants.EMAIL
                        ).toString()
                    )
                )

            }
        }

    }

    override fun onPause() {
        super.onPause()
        isPaused = true
        isCancelled = false
    }

    override fun onResume() {
        super.onResume()
        timer(resumeFromMillis, countDownInterval).start()
        isPaused = false
        isCancelled = false
    }


    private fun timer(millisInFuture: Long, countDownInterval: Long): CountDownTimer {
        return object : CountDownTimer(millisInFuture, countDownInterval) {
            @SuppressLint("SetTextI18n")
            override fun onTick(millisUntilFinished: Long) {
                val timeRemaining = "remaining seconds ${millisUntilFinished / 1000}"
                expireTime = millisUntilFinished.toInt()
                // binding.tvTime.text = "$timeRemaining seconds"

                when {
                    isPaused -> {
                        viewDataBinding?.tvTime?.text = "${viewDataBinding?.tvTime?.text}\nPaused"
                        resumeFromMillis = millisUntilFinished
                        cancel()
                    }
                    isCancelled -> {
                        viewDataBinding?.tvTime?.text =
                            "${viewDataBinding?.tvTime?.text}\nStopped.(Cancelled)"
                        cancel()
                    }
                    else -> {
                        viewDataBinding?.tvTime?.text = timeRemaining
                    }
                }
            }

            @SuppressLint("SetTextI18n")
            override fun onFinish() {
                viewDataBinding?.apply {

                    if (expireTime == 0) {
                        viewDataBinding?.tvResendOTP?.visibility = View.GONE
                    } else {
                        viewDataBinding?.tvResendOTP?.visibility = View.VISIBLE
                    }
                    tvTime.text = "00:00:00"

                }
            }
        }

    }


    private fun callAPi(body: OTPModel) {
        homeActivity?.let {
            ApiCall.verifyOTPApi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    if (mResponse.body()?.status == true) {
                        homeActivity?.clearAllStack()
                        SharedPrefClass().putObject(
                            it, GlobalConstants.USERID, mResponse.body()?.user_id
                        )

                        if (from == 1) {
                            homeActivity?.displayItAddStack(ResetPasswordFragment())
                        } else {
                            homeActivity?.displayItAddStack(CreateProfileFragment())
                        }
                    }

                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    private fun callResendAPi(body: ResendOTPModel) {
        homeActivity?.let {
            ApiCall.resendOTPApi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    if (mResponse.body()?.status == true) {
                        showToast(mResponse.body()?.message)
                    }
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

}