package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import com.tribeplay.R
import com.tribeplay.databinding.FragmentLeagueDetailBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.base.BaseFragment


class LeagueDetailFragment : BaseFragment<FragmentLeagueDetailBinding>() {

    override fun getLayoutId(): Int {
        return R.layout.fragment_league_detail
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarTtl = getString(R.string.lLeague_detail)
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true

        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

        }


    }


}