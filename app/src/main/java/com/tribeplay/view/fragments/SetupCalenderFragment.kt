package com.tribeplay.view.fragments

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.tribeplay.R
import com.tribeplay.databinding.FragmentSetCalenderBinding
import com.tribeplay.databinding.ItemActivitiesBinding
import com.tribeplay.databinding.ItemImgTvBoxBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.adapters.RecyclerCallback
import com.tribeplay.view.adapters.RecyclerViewGenricAdapter
import com.tribeplay.view.base.BaseFragment


class SetupCalenderFragment : BaseFragment<FragmentSetCalenderBinding>() {


    override fun getLayoutId(): Int {
        return R.layout.fragment_set_calender
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.toolBarleftTlShow = true
        toolBarModel.toolBarCrossIconShow = true
        toolBarModel.toolBarLetfTtl = getString(R.string.sSet_Calender)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


//        homeActivity?.viewDataBinding?.imgCrossbtn?.setOnClickListener {
//            viewDataBinding?.viewFlipper?.showPrevious()
//        }

        viewDataBinding?.apply {
            recyclerView.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            setAdapter()

            tvReminder.setOnClickListener {
                displayItAddStack(ReminderFragment())
            }

            btnCall.setOnClickListener {

            }

        }
    }

    fun setAdapter() {
        val list = ArrayList<String>()
        list.add("Cricket")
        list.add("Tennis")
        list.add("Racket")
        list.add("BasketBall")


        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemImgTvBoxBinding>(
            list,
            R.layout.item_img_tv_box, object :
                RecyclerCallback<ItemImgTvBoxBinding, String> {
                override fun bindData(
                    binder: ItemImgTvBoxBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {
                        txtName.text = model
                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


}