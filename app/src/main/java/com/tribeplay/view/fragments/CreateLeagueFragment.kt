package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tribeplay.R
import com.tribeplay.databinding.FragmentCreateLeaguesBinding
import com.tribeplay.databinding.ItemImgTvBoxBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.adapters.RecyclerCallback
import com.tribeplay.view.adapters.RecyclerViewGenricAdapter
import com.tribeplay.view.base.BaseFragment


class CreateLeagueFragment : BaseFragment<FragmentCreateLeaguesBinding>() {

    override fun getLayoutId(): Int {
        return R.layout.fragment_create_leagues
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarTtl = getString(R.string.create_league)
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            recyclerView.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            recyclerView1.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            recyclerViewLevel.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            recyclerViewTeam.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            setAdapter(recyclerView)
            setAdapter(recyclerView1)
            setAdapter(recyclerViewLevel)
            setAdapter(recyclerViewTeam)


            btnCall.setOnClickListener {

                if(viewFlipper.displayedChild==4){
                    popBack()
                }else{
                    viewFlipper.showNext()
                }
            }


        }
    }


    fun setAdapter(recyclerView: RecyclerView) {
        val list = ArrayList<String>()
        list.add("Cricket")
        list.add("Tennis")
        list.add("Racket")
        list.add("BasketBall")


        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemImgTvBoxBinding>(
            list,
            R.layout.item_img_tv_box, object :
                RecyclerCallback<ItemImgTvBoxBinding, String> {
                override fun bindData(
                    binder: ItemImgTvBoxBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {
                        txtName.text = model
                    }
                }
            })
        recyclerView.adapter = rvAdapProgress

    }


}