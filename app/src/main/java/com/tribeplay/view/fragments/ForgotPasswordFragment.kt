package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.tribeplay.R
import com.tribeplay.databinding.FragmentForgotPassBinding
import com.tribeplay.model.GenericModel
import com.tribeplay.model.ResendOTPModel
import com.tribeplay.model.ToolBarModel
import com.tribeplay.utils.GlobalConstants
import com.tribeplay.utils.SharedPrefClass
import com.tribeplay.utils.UtilsFunctions
import com.tribeplay.view.base.BaseFragment
import retrofit2.Response

private const val TAG = "ForgotPasswordFragment"

class ForgotPasswordFragment : BaseFragment<FragmentForgotPassBinding>() {


    override fun getLayoutId(): Int {
        return R.layout.fragment_forgot_pass
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            btnCall.setOnClickListener {
                if (isValidate()) {
                    callAPi(
                        ResendOTPModel(
                            etEmail.text.toString()
                        )
                    )

                }
            }
        }
    }

    private fun callAPi(body: ResendOTPModel) {
        homeActivity?.let {
            ApiCall.resendOTPApi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    if (mResponse.body()?.status == true) {
                        //showToast(mResponse.body()?.message)
                        //val=  mResponse.body()?.otp
                        viewDataBinding?.apply {
                            val email = etEmail.text.toString()
                            homeActivity?.displayItAddStack(OtpverificationFragment(1, email))
                        }
                    }
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    private fun isValidate(): Boolean {
        viewDataBinding?.apply {
            if (etEmail.text.isNullOrEmpty()) {
                showToast(getString(R.string.valid_enter_email))
                return false
            } else if (!UtilsFunctions.isValidEmail(etEmail.text.toString())) {
                showToast(getString(R.string.valid_email))
                return false
            } else {
                return true
            }


        }
        return true
    }

}