package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.tribeplay.R
import com.tribeplay.databinding.FragmentMsgBinding
import com.tribeplay.databinding.ItemChatMsgBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.adapters.RecyclerCallback
import com.tribeplay.view.adapters.RecyclerViewGenricAdapter
import com.tribeplay.view.base.BaseFragment


class MessageFragment(val chkTab: Int) : BaseFragment<FragmentMsgBinding>() {


    private var rvAdapProgress: RecyclerViewGenricAdapter<String, ItemChatMsgBinding>? = null

    override fun getLayoutId(): Int {
        return R.layout.fragment_msg
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.chatShow = true
        if (chkTab == 2) {
            toolBarModel.chatViewDetail = true
            toolBarModel.imgCalenderShow = false
            toolBarModel.imgGrpUserShow = false
        } else if (chkTab == 1) {
            toolBarModel.chatViewDetail = false
            toolBarModel.imgCalenderShow = true
            toolBarModel.imgGrpUserShow = true
        } else {
            toolBarModel.chatViewDetail = false
            toolBarModel.imgCalenderShow = true
            toolBarModel.imgGrpUserShow = false
        }
        return toolBarModel
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        homeActivity?.viewDataBinding?.apply {
            tvChatNm?.setOnClickListener {
                displayItAddStack(ChatSettingsFragment(chkTab))
            }

            tvViewDetail.setOnClickListener {
                displayItAddStack(LeagueAllDetailFragment(1))
            }

            if (chkTab == 2) {
                tvViewDetail.visibility = View.VISIBLE
            } else if (chkTab == 1) {
                imgGrpUser.visibility = View.VISIBLE
                imgCalender.visibility = View.VISIBLE
            } else {
                imgGrpUser.visibility = View.GONE
                imgCalender.visibility = View.VISIBLE

            }
        }



        viewDataBinding?.apply {
            recyclerView.layoutManager =
                LinearLayoutManager(context)
            setAdapter()
        }

    }


    fun setAdapter() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        rvAdapProgress = RecyclerViewGenricAdapter<String, ItemChatMsgBinding>(
            list,
            R.layout.item_chat_msg, object :
                RecyclerCallback<ItemChatMsgBinding, String> {
                override fun bindData(
                    binder: ItemChatMsgBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {


                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


}