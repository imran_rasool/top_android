package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayout
import com.tribeplay.R
import com.tribeplay.databinding.*
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.adapters.BottomFragmentCallback
import com.tribeplay.view.adapters.RecyclerCallback
import com.tribeplay.view.adapters.RecyclerViewGenricAdapter
import com.tribeplay.view.base.BaseFragment


class LeaguesFragment : BaseFragment<FragmentLeaguesBinding>() {


    private var bottomSheetDialog: ActionBottomDialogFragment<String, DialogEditLeagueDetailBinding>? =
        null
    private var rvAdapProgress: RecyclerViewGenricAdapter<String, ItemLeaguesBinding>? = null
    private var selectPosition: Int = 0

    override fun getLayoutId(): Int {
        return R.layout.fragment_leagues
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarTtl = getString(R.string.lLeague)
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = false
        toolBarModel.bottomNav = true
        toolBarModel.notiIconShow = true
        toolBarModel.toolBarProfileShow = true
        toolBarModel.bookMarkShow = true
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            recyclerView.layoutManager =
                LinearLayoutManager(context)
            setAdapter(0)



            if (tabLayout.tabCount <= 0) {
                tabLayout.addTab(tabLayout.newTab().setText(R.string.mMy_League))
                tabLayout.addTab(tabLayout.newTab().setText(R.string.mAvailable_League))

            }

            llUpcoming.setOnClickListener {
                llUpcoming.setBackgroundResource(R.drawable.round_solid_blue_lite_btn)
                tvupcoming.setTextColor(setColor(R.color.colorBtn))
                tvPast.setTextColor(setColor(R.color.black))
                llPast.setBackgroundResource(R.drawable.round_solid_gray_lite_btn)
                setAdapter(0)
            }

            tvCraeteLeague.setOnClickListener {
                homeActivity?.displayItAddStack(CreateLeagueFragment())
            }

            tvLeagueInvite.setOnClickListener {
                homeActivity?.displayItAddStack(LeagueInviteFragment())
            }

            llPast.setOnClickListener {
                llPast.setBackgroundResource(R.drawable.round_solid_blue_lite_btn)
                llUpcoming.setBackgroundResource(R.drawable.round_solid_gray_lite_btn)
                tvPast.setTextColor(setColor(R.color.colorBtn))
                tvupcoming.setTextColor(setColor(R.color.black))
                setAdapter(1)
            }


            tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab?) {
                    selectPosition = tab?.position ?: 0
                    if (selectPosition == 1) {
                        llMainSport.visibility = View.VISIBLE
                        llMainUpcoming.visibility = View.GONE
                        rlCraeteGrp.visibility = View.GONE
                        setAdapter(2)
                    } else {
                        llMainUpcoming.visibility = View.VISIBLE
                        rlCraeteGrp.visibility = View.VISIBLE
                        llMainSport.visibility = View.GONE
                        llUpcoming.setBackgroundResource(R.drawable.round_solid_blue_lite_btn)
                        tvupcoming.setTextColor(setColor(R.color.colorBtn))
                        tvPast.setTextColor(setColor(R.color.black))
                        llPast.setBackgroundResource(R.drawable.round_solid_gray_lite_btn)
                        setAdapter(0)
                    }

                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {

                }

                override fun onTabReselected(tab: TabLayout.Tab?) {

                }

            })


        }


    }


    fun setAdapter(i: Int) {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        rvAdapProgress = RecyclerViewGenricAdapter<String, ItemLeaguesBinding>(
            list,
            R.layout.item_leagues, object :
                RecyclerCallback<ItemLeaguesBinding, String> {
                override fun bindData(
                    binder: ItemLeaguesBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                        if (i == 2) {
                            llEdit.visibility = View.GONE
                            tvCreated.visibility = View.GONE
                            tvDateTime.visibility = View.GONE
                            tvCollectRs.visibility = View.GONE
                            imgRibbon.visibility = View.VISIBLE
                            tvEqu.visibility = View.VISIBLE
                            tvOtherLoc.visibility = View.VISIBLE

                        } else if (i == 1) {
                            llEdit.visibility = View.GONE
                            tvCreated.visibility = View.VISIBLE
                            tvCollectRs.visibility = View.VISIBLE
                            tvDateTime.visibility = View.VISIBLE
                            imgRibbon.visibility = View.GONE
                            tvEqu.visibility = View.GONE
                            tvOtherLoc.visibility = View.GONE
                        } else {
                            llEdit.visibility = View.VISIBLE
                            tvCreated.visibility = View.VISIBLE
                            tvDateTime.visibility = View.VISIBLE
                            tvCollectRs.visibility = View.VISIBLE
                            imgRibbon.visibility = View.GONE
                            tvEqu.visibility = View.GONE
                            tvOtherLoc.visibility = View.GONE
                        }
                        linearMain.setOnClickListener {
                            if (selectPosition == 1)
                                displayItAddStack(LeagueDetailFragment())
                            else
                                displayItAddStack(LeagueAllDetailFragment(0))
                        }

                        tvEdit.setOnClickListener {
                            dialogCall()
                        }
                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


    fun setAdapter() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemMarkInterestBinding>(
            list,
            R.layout.item_mark_interest, object :
                RecyclerCallback<ItemMarkInterestBinding, String> {
                override fun bindData(
                    binder: ItemMarkInterestBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {
                        btnCall.visibility = View.GONE

                        linearMain.setOnClickListener {
                            displayItAddStack(LeagueDetailFragment())
                        }
                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


    fun dialogCall() {
        bottomSheetDialog =
            ActionBottomDialogFragment<String, DialogEditLeagueDetailBinding>(
                R.layout.dialog_edit_league_detail, object :
                    BottomFragmentCallback<DialogEditLeagueDetailBinding, String> {
                    override fun bindData(binder: DialogEditLeagueDetailBinding) {
                        binder.apply {


                            btnCancel.setOnClickListener {
                                bottomSheetDialog?.dismiss()
                            }
                        }
                    }
                })

        bottomSheetDialog?.show(
            homeActivity?.supportFragmentManager!!,
            ActionBottomDialogFragment::class.java.canonicalName
        )
    }


}