package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.tribeplay.R
import com.tribeplay.databinding.FragmentLeaguesInviteBinding
import com.tribeplay.databinding.ItemLeaguesBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.adapters.RecyclerCallback
import com.tribeplay.view.adapters.RecyclerViewGenricAdapter
import com.tribeplay.view.base.BaseFragment


class LeagueInviteFragment : BaseFragment<FragmentLeaguesInviteBinding>() {

    private var rvAdapProgress: RecyclerViewGenricAdapter<String, ItemLeaguesBinding>? = null

    override fun getLayoutId(): Int {
        return R.layout.fragment_leagues_invite
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarTtl = getString(R.string.lLeague_Invite)
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewDataBinding?.apply {
            recyclerView.layoutManager =
                LinearLayoutManager(context)
            setAdapter(0)
        }
    }

    fun setAdapter(i: Int) {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        rvAdapProgress = RecyclerViewGenricAdapter<String, ItemLeaguesBinding>(
            list,
            R.layout.item_leagues, object :
                RecyclerCallback<ItemLeaguesBinding, String> {
                override fun bindData(
                    binder: ItemLeaguesBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                        llEdit.visibility = View.GONE
                        tvCreated.visibility = View.GONE
                        tvDateTime.visibility = View.GONE
                        tvCollectRs.visibility = View.GONE
                        imgRibbon.visibility = View.VISIBLE
                        tvEqu.visibility = View.VISIBLE
                        tvOtherLoc.visibility = View.VISIBLE

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }

}