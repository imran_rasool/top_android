package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import com.tribeplay.R
import com.tribeplay.databinding.FragmentDownloadBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.base.BaseFragment


class DownloadRecieptFragment : BaseFragment<FragmentDownloadBinding>() {


    override fun getLayoutId(): Int {
        return R.layout.fragment_download
    }


    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.toolBarTtl = getString(R.string.dDownload_Receipt)
        toolBarModel.backBtn = true
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

        }


    }


}