package com.tribeplay.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.MotionEvent
import android.view.View
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.orhanobut.hawk.Hawk
import com.tribeplay.R
import com.tribeplay.application.TribesPlayApplication
import com.tribeplay.databinding.FragmentLoginBinding
import com.tribeplay.model.GenericModel
import com.tribeplay.model.LoginModel
import com.tribeplay.model.ToolBarModel
import com.tribeplay.utils.GlobalConstants
import com.tribeplay.utils.SharedPrefClass
import com.tribeplay.view.base.BaseFragment
import retrofit2.Response

private const val TAG = "LoginFragment"

class LoginFragment : BaseFragment<FragmentLoginBinding>() {

    var isPasswordVisibe = false

    override fun getLayoutId(): Int {
        return R.layout.fragment_login
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (!Hawk.isBuilt()) {
            Hawk.init(requireContext()).build()
        }

        viewDataBinding?.apply {

            btnlogin.setOnClickListener {

                if (isValidate()) {
                    callAPi(
                        LoginModel(
                            etEmail.text.toString(),
                            etPass.text.toString()
                        )
                    )
                }

            }

            etPass.setOnTouchListener(View.OnTouchListener { v, event ->
                val DRAWABLE_RIGHT = 2
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX >= etPass.right - etPass.compoundDrawables[DRAWABLE_RIGHT].bounds.width()
                    ) {
                        isPasswordVisibe = !isPasswordVisibe

                        if (isPasswordVisibe) {
                            etPass.transformationMethod =
                                HideReturnsTransformationMethod.getInstance();
                            etPass.setCompoundDrawablesWithIntrinsicBounds(
                                0,
                                0,
                                R.drawable.ic_icon_visibility,
                                0
                            );
                        } else {
                            etPass.transformationMethod =
                                PasswordTransformationMethod.getInstance();
                            etPass.setCompoundDrawablesWithIntrinsicBounds(
                                0,
                                0,
                                R.drawable.ic_resource_private,
                                0
                            );
                        }

                        return@OnTouchListener true
                    }
                }
                false
            })

            underlineText(tvSignUp)

            tvForgotPass.setOnClickListener {
                // homeActivity?.displayItAddStack(CreateProfile3Fragment())
                homeActivity?.displayItAddStack(ForgotPasswordFragment())
            }

            tvSignUp.setOnClickListener {
                // homeActivity?.displayItAddStack(CreateProfile4Fragment())
                homeActivity?.displayItAddStack(SignUpFragment())
            }
        }
    }

    private fun callAPi(body: LoginModel) {
        homeActivity?.let {
            ApiCall.loginApi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    //Log.d(TAG, "onSuccess: ${Gson().toJson(mResponse.body()?.data)}")
                    val model = mResponse.body()?.data!![0]
                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.EMAIL,
                        mResponse.body()?.data?.get(0)?.email.toString()
                    )
                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.ACCESS_TOKEN,
                        mResponse.body()?.data!![0].jwt_token
                    )

                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.FIRST_NAME,
                        mResponse.body()?.data?.get(0)?.fname.toString()
                    )
                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.PREFERENCE,
                        mResponse.body()?.data?.get(0)?.preferences.toString()
                    )
                    val userId: String = mResponse.body()?.data!![0].tbl_users_id.toString()
                    Hawk.put(GlobalConstants.USERID1, userId)
                    if (mResponse.body()?.data!![0].tbl_users_id != 0) {
                        SharedPrefClass().putObject(
                            it,
                            GlobalConstants.USERID,
                            mResponse.body()?.data!![0].tbl_users_id
                        )
                    }


                    homeActivity?.clearAllStack()

                    when {
                        model.otp_verify != 1 -> {
                            homeActivity?.displayItAddStack(
                                OtpverificationFragment(
                                    0,
                                    SharedPrefClass().getPrefValue(
                                        requireContext(), GlobalConstants.EMAIL
                                    ).toString()
                                )
                            )
                        }
                        model.create_profile != 1 -> {
                            homeActivity?.displayItAddStack(CreateProfileFragment())
                        }
                        model.sport != 1 -> {
                            homeActivity?.displayItAddStack(CreateProfile3Fragment())
                        }
                        model.preferences != 1 -> {
                            homeActivity?.displayItAddStack(CreateProfile4Fragment())
                        }
                        else -> {
                            //  homeActivity?.displayItAddStack(HomeFragment())
                            homeActivity?.viewDataBinding?.bottomNavigationView?.selectedItemId =
                                R.id.action_grp

                        }
                    }

                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    private fun isValidate(): Boolean {
        viewDataBinding?.apply {
            if (etEmail.text.isNullOrEmpty()) {
                showToast(getString(R.string.enter_ur_email))
                return false
            } else if (etPass.text.isNullOrEmpty()) {
                showToast(getString(R.string.enter_password))
                return false
            } else {
                return true
            }
        }
        return true
    }

}