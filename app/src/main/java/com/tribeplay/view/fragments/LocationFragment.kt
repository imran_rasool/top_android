package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.os.bundleOf
import com.google.android.gms.common.api.Status
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.tribeplay.R
import com.tribeplay.databinding.FragmentLocationBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.utils.GlobalConstants
import com.tribeplay.utils.GlobalConstants.requestKey
import com.tribeplay.utils.setFragmentResult
import com.tribeplay.view.base.BaseFragment


private const val TAG = "LocationFragment"

class LocationFragment : BaseFragment<FragmentLocationBinding>() {

    override fun getLayoutId(): Int {
        return R.layout.fragment_location
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false

        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val apiKey = getString(R.string.GOOGLE_MAP_KEY_NEW)
        viewDataBinding?.apply {

            if (apiKey.isEmpty()) {
                tvErrorMsg.text = getString(R.string.error)
                return
            }
        }

        // Initializing the Places API
        // with the help of our API_KEY
        if (!Places.isInitialized()) {
            Places.initialize(requireContext(), apiKey)
        }
        // Initialize Autocomplete Fragments
        // from the main activity layout file
        val autocompleteSupportFragment1 =
            requireActivity().supportFragmentManager.findFragmentById(R.id.autocomplete_fragment1) as AutocompleteSupportFragment?

        // Information that we wish to fetch after typing
        // the location and clicking on one of the options
        autocompleteSupportFragment1?.setPlaceFields(
            listOf(

                Place.Field.NAME,
                Place.Field.ADDRESS,
                Place.Field.PHONE_NUMBER,
                Place.Field.LAT_LNG,
                Place.Field.OPENING_HOURS,
                Place.Field.RATING,
                Place.Field.USER_RATINGS_TOTAL

            )
        )

        // Display the fetched information after clicking on one of the options
        autocompleteSupportFragment1?.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {

                // Text view where we will
                // append the information that we fetch
                //val textView = findViewById<TextView>(R.id.tv1)

                // Information about the place
                val name = place.name
                val address = place.address
                val phone = place.phoneNumber.toString()
                val latlng = place.latLng
                val latitude = latlng?.latitude
                val longitude = latlng?.longitude

                val isOpenStatus: String = if (place.isOpen == true) {
                    "Open"
                } else {
                    "Closed"
                }
                setFragmentResult(requestKey, bundleOf(GlobalConstants.FILTER_OBJECT to address))
                popBack()
                val rating = place.rating
                val userRatings = place.userRatingsTotal

                // textView.text = "Name: $name \nAddress: $address \nPhone Number: $phone \n" +
                //       "Latitude, Longitude: $latitude , $longitude \nIs open: $isOpenStatus \n" +
                //     "Rating: $rating \nUser ratings: $userRatings"
            }

            override fun onError(status: Status) {
                showToast("Some error occurred")
            }
        })

    }


}