package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.tribeplay.R
import com.tribeplay.databinding.DialogAssignPrevilageBinding
import com.tribeplay.databinding.FragmentAssistantMaanagerBinding
import com.tribeplay.databinding.ItemAssistantmanagerBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.adapters.BottomFragmentCallback
import com.tribeplay.view.adapters.RecyclerCallback
import com.tribeplay.view.adapters.RecyclerViewGenricAdapter
import com.tribeplay.view.base.BaseFragment


class AssistantManagerFragment : BaseFragment<FragmentAssistantMaanagerBinding>() {


    private var bottomSheetFilterDialog: ActionBottomDialogFragment<String, DialogAssignPrevilageBinding>? =
        null
    private var rvAdapProgress: RecyclerViewGenricAdapter<String, ItemAssistantmanagerBinding>? =
        null

    override fun getLayoutId(): Int {
        return R.layout.fragment_assistant_maanager
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarTtl = getString(R.string.assistant_Manager)
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            recyclerView.layoutManager =
                LinearLayoutManager(context)
            setAdapter()

            btnCall.setOnClickListener {
                dialogPrevilage()
            }

        }
    }


    fun setAdapter() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        rvAdapProgress = RecyclerViewGenricAdapter<String, ItemAssistantmanagerBinding>(
            list,
            R.layout.item_assistantmanager, object :
                RecyclerCallback<ItemAssistantmanagerBinding, String> {
                override fun bindData(
                    binder: ItemAssistantmanagerBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {


                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }

    fun dialogPrevilage() {
        bottomSheetFilterDialog =
            ActionBottomDialogFragment<String, DialogAssignPrevilageBinding>(
                R.layout.dialog_assign_previlage, object :
                    BottomFragmentCallback<DialogAssignPrevilageBinding, String> {
                    override fun bindData(binder: DialogAssignPrevilageBinding) {
                        binder.apply {

                            btnCall.setOnClickListener {
                                bottomSheetFilterDialog?.dismiss()
                                homeActivity?.displayItAddStack(CraeteGroupFragment())
                            }
                            btnCancel.setOnClickListener {
                                bottomSheetFilterDialog?.dismiss()
                            }
                        }
                    }
                })

        bottomSheetFilterDialog?.show(
            homeActivity?.supportFragmentManager!!,
            ActionBottomDialogFragment::class.java.canonicalName
        )
    }


}