package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.tribeplay.R
import com.tribeplay.databinding.FragmentChatSettingsBinding
import com.tribeplay.databinding.ItemGameDateTimeBinding
import com.tribeplay.databinding.ItemGrpMemeberBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.adapters.RecyclerCallback
import com.tribeplay.view.adapters.RecyclerViewGenricAdapter
import com.tribeplay.view.base.BaseFragment


class ChatSettingsFragment(val chkTab: Int) : BaseFragment<FragmentChatSettingsBinding>() {


    private var rvAdapProgress: RecyclerViewGenricAdapter<String, ItemGameDateTimeBinding>? = null
    private var rvAdapProgress1: RecyclerViewGenricAdapter<String, ItemGrpMemeberBinding>? = null
    private var selectPosition: Int = 0

    override fun getLayoutId(): Int {
        return R.layout.fragment_chat_settings
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarTtl = getString(R.string.cChat_Settings)
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            recyclerView.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

            recyclerViewCap.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

            recyclerViewMembers.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

            recyclerViewTab.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            setAdapter(recyclerView)
            setAdapter(recyclerViewCap)
            setAdapter(recyclerViewMembers)
            setAdapter(0)

            if (chkTab == 2) {
                llChat.visibility = View.GONE
                llcaptains.visibility = View.VISIBLE
            } else {
                llChat.visibility = View.VISIBLE
                llcaptains.visibility = View.GONE
            }



            if (tabLayout.tabCount <= 0) {
                tabLayout.addTab(tabLayout.newTab().setText(R.string.pPast))
                tabLayout.addTab(tabLayout.newTab().setText(R.string.uUpcoming))

            }


            tvSetCalender.setOnClickListener {
                displayItAddStack(SetupCalenderFragment())
            }


            tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab?) {
                    selectPosition = tab?.position ?: 0

                    setAdapter(selectPosition)
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {

                }

                override fun onTabReselected(tab: TabLayout.Tab?) {

                }

            })


        }


    }

    fun setAdapter(recyclerView: RecyclerView) {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        rvAdapProgress1 = RecyclerViewGenricAdapter<String, ItemGrpMemeberBinding>(
            list,
            R.layout.item_grp_memeber, object :
                RecyclerCallback<ItemGrpMemeberBinding, String> {
                override fun bindData(
                    binder: ItemGrpMemeberBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {


                    }
                }
            })

        recyclerView.adapter = rvAdapProgress1

    }

    fun setAdapter(i: Int) {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        rvAdapProgress = RecyclerViewGenricAdapter<String, ItemGameDateTimeBinding>(
            list,
            R.layout.item_game_date_time, object :
                RecyclerCallback<ItemGameDateTimeBinding, String> {
                override fun bindData(
                    binder: ItemGameDateTimeBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {


                    }
                }
            })
        viewDataBinding?.apply {
            recyclerViewTab.adapter = rvAdapProgress
        }
    }


}