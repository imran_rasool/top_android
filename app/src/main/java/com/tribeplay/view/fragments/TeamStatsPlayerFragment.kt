package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tribeplay.R
import com.tribeplay.databinding.FragmentTeamStatsBinding
import com.tribeplay.databinding.ItemMarkInterestBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.adapters.RecyclerCallback
import com.tribeplay.view.adapters.RecyclerViewGenricAdapter
import com.tribeplay.view.base.BaseFragment


class TeamStatsPlayerFragment : BaseFragment<FragmentTeamStatsBinding>() {


    override fun getLayoutId(): Int {
        return R.layout.fragment_team_stats
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.toolBarTtl = getString(R.string.cCheck_Tea_Stats)
        toolBarModel.backBtn = true
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            recyclerViewActive.layoutManager =
                LinearLayoutManager(context)
            setAdapter(recyclerViewActive)

            recyclerViewSubs.layoutManager =
                LinearLayoutManager(context)
            setAdapter(recyclerViewSubs)
            tvPickShirt.setOnClickListener {
                displayItAddStack(PickShirtFragment())
            }

        }


    }


    fun setAdapter(recyclerViewActive: RecyclerView) {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemMarkInterestBinding>(
            list,
            R.layout.item_mark_interest, object :
                RecyclerCallback<ItemMarkInterestBinding, String> {
                override fun bindData(
                    binder: ItemMarkInterestBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                    }
                }
            })
        //  viewDataBinding?.apply {
        recyclerViewActive.adapter = rvAdapProgress
        //  }
    }

}