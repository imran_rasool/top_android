package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import com.tribeplay.R
import com.tribeplay.databinding.FragmentSetttingsBinding
import com.tribeplay.databinding.FragmentSetttingsNotificationsBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.base.BaseFragment


class SettingsNotificationsFragment : BaseFragment<FragmentSetttingsNotificationsBinding>() {


    override fun getLayoutId(): Int {
        return R.layout.fragment_setttings_notifications
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarTtl = getString(R.string.nNotification_settings)
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {


        }


    }


}