package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.tribeplay.R
import com.tribeplay.databinding.FragmentPaymentBinding
import com.tribeplay.databinding.ItemPaymentBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.adapters.RecyclerCallback
import com.tribeplay.view.adapters.RecyclerViewGenricAdapter
import com.tribeplay.view.base.BaseFragment


class TransactionFragment : BaseFragment<FragmentPaymentBinding>() {

    var list = ArrayList<String>()

    override fun getLayoutId(): Int {
        return R.layout.fragment_payment
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarTtl = getString(R.string.ur_transaction)
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true

        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        list.add(getString(R.string.lLeague_detail))
        list.add(getString(R.string.cChat))
        list.add(getString(R.string.pParticipants))
        list.add(getString(R.string.tTeam))
        list.add(getString(R.string.mMatches))
        list.add(getString(R.string.rResults))
        list.add(getString(R.string.pPayments))



        setAdapterPayment()


    }


    fun setAdapterPayment() {

        viewDataBinding?.recyclerView?.layoutManager =
            LinearLayoutManager(context)
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        var rvAdapProgress = RecyclerViewGenricAdapter<String, ItemPaymentBinding>(
            list,
            R.layout.item_payment, object :
                RecyclerCallback<ItemPaymentBinding, String> {
                override fun bindData(
                    binder: ItemPaymentBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {
                        imgDownload.setOnClickListener {
                            displayItAddStack(DownloadRecieptFragment())
                        }


                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


}