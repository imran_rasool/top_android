package com.tribeplay.view.fragments

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.tribeplay.R
import com.tribeplay.databinding.DialogBottomSheetFilterBinding
import com.tribeplay.databinding.DialogBottomSheetSelectSportsBinding
import com.tribeplay.databinding.FragmentHomeBinding
import com.tribeplay.databinding.ItemImgTvBoxBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.utils.CirleImageView
import com.tribeplay.view.adapters.BottomFragmentCallback
import com.tribeplay.view.adapters.RecyclerCallback
import com.tribeplay.view.adapters.RecyclerViewGenricAdapter
import com.tribeplay.view.base.BaseFragment


class HomeFragment : BaseFragment<FragmentHomeBinding>() {

    // private var mapFragment: SupportMapFragment? = null
    private var bottomSheetDialog: ActionBottomDialogFragment<String, DialogBottomSheetSelectSportsBinding>? =
        null
    private var bottomSheetFilterDialog: ActionBottomDialogFragment<String, DialogBottomSheetFilterBinding>? =
        null
    private var mMap: GoogleMap? = null


    override fun getLayoutId(): Int {
        return R.layout.fragment_home
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = false
        toolBarModel.bottomNav = true
        toolBarModel.notiIconShow = true
        toolBarModel.toolBarProfileShow = true
        toolBarModel.locShow = true
        toolBarModel.toolBarTtl = getString(R.string.eExplore)
        toolBarModel.bookMarkShow = true
        toolBarModel.locShow = true
        return toolBarModel
    }

    override fun onResume() {
        super.onResume()
        viewDataBinding?.map1?.onResume()
    }

    override fun onPause() {
        super.onPause()
        viewDataBinding?.map1?.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewDataBinding?.map1?.onDestroy()

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            map1.onCreate(savedInstanceState)
            map1.getMapAsync(object : OnMapReadyCallback {
                override fun onMapReady(googleMap: GoogleMap) {
                    mMap = googleMap
                    // Add a marker in Sydney and move the camera
                    val latLng = LatLng(-34.0, 151.0)
//                    mMap.addMarker(
//                        MarkerOptions()
//                            .position(latLng)
//                            .title("Marker in Sydney")
//                    )


                    mMap?.addMarker(
                        MarkerOptions().position(latLng).icon(
                            BitmapDescriptorFactory.fromBitmap(
                                createCustomMarker(context!!, R.drawable.img2, "Manish")
                            )
                        )
                    )?.setTitle("iPragmatech Solutions Pvt Lmt")

                    mMap?.moveCamera(CameraUpdateFactory.newLatLng(latLng))
                    mMap?.setOnMarkerClickListener {
                        homeActivity?.displayItAddStack(PlayerDetailFragment())
                        return@setOnMarkerClickListener true
                    }
                }

            })

            llSport.setOnClickListener {
                dialogSelectSportz()
            }

            rlFilter.setOnClickListener {
                dialogFilter()
            }

            llCreateGrp.setOnClickListener {
                homeActivity?.displayItAddStack(DropInterestFragment(getString(R.string.dDrop_interest)))
            }

        }

    }


    fun createCustomMarker(context: Context, @DrawableRes resource: Int, _name: String?): Bitmap? {
        val marker: View =
            (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                R.layout.custom_marker_layout,
                null
            )
        val markerImage: CirleImageView =
            marker.findViewById<View>(R.id.user_dp) as CirleImageView
        markerImage.setImageResource(resource)
//        val txt_name = marker.findViewById<View>(R.id.name) as TextView
//        txt_name.text = _name
        val displayMetrics = DisplayMetrics()
        (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
        marker.layoutParams = ViewGroup.LayoutParams(52, ViewGroup.LayoutParams.WRAP_CONTENT)
        marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
        marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
        marker.buildDrawingCache()
        val bitmap = Bitmap.createBitmap(
            marker.measuredWidth,
            marker.measuredHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        marker.draw(canvas)
        return bitmap
    }


    fun dialogSelectSportz() {
        bottomSheetDialog =
            ActionBottomDialogFragment<String, DialogBottomSheetSelectSportsBinding>(
                R.layout.dialog_bottom_sheet_select_sports, object :
                    BottomFragmentCallback<DialogBottomSheetSelectSportsBinding, String> {
                    override fun bindData(binder: DialogBottomSheetSelectSportsBinding) {

                        binder.apply {
                            val layoutManager = FlexboxLayoutManager(context)
                            layoutManager.flexDirection = FlexDirection.ROW
                            layoutManager.justifyContent = JustifyContent.CENTER
                            recyclerView.setLayoutManager(layoutManager)
                            setAdapter(recyclerView)

                            btnCall.setOnClickListener {
                                bottomSheetDialog?.dismiss()
                                //  pair.second.dismiss()
                            }

                            btnCancel.setOnClickListener {
                                bottomSheetDialog?.dismiss()
                            }

                            llSport.setOnClickListener {
                                homeActivity?.displayItAddStack(CreateProfile3Fragment())
                            }
                        }
                    }
                })

        bottomSheetDialog?.show(
            homeActivity?.supportFragmentManager!!,
            ActionBottomDialogFragment::class.java.canonicalName
        )
    }


    fun dialogFilter() {
        bottomSheetFilterDialog =
            ActionBottomDialogFragment<String, DialogBottomSheetFilterBinding>(
                R.layout.dialog_bottom_sheet_filter, object :
                    BottomFragmentCallback<DialogBottomSheetFilterBinding, String> {
                    override fun bindData(binder: DialogBottomSheetFilterBinding) {
                        binder.apply {

                            recyclerView.layoutManager =
                                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                            setAdapterFilter(recyclerView)
                            btnCall.setOnClickListener {
                                bottomSheetDialog?.dismiss()
                            }
                            btnCancel.setOnClickListener {
                                bottomSheetDialog?.dismiss()
                            }
                        }
                    }
                })

        bottomSheetFilterDialog?.show(
            homeActivity?.supportFragmentManager!!,
            ActionBottomDialogFragment::class.java.canonicalName
        )
    }


    fun setAdapterFilter(recyclerView: RecyclerView) {
        val list = ArrayList<String>()
        list.add(getString(R.string.bBeginner))
        list.add(getString(R.string.iIntermediate))
        list.add(getString(R.string.aAdvanced))
        list.add(getString(R.string.any))


        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemImgTvBoxBinding>(
            list,
            R.layout.item_img_tv_box, object :
                RecyclerCallback<ItemImgTvBoxBinding, String> {
                override fun bindData(
                    binder: ItemImgTvBoxBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {
                        txtName.text = model


                    }
                }
            })
        recyclerView.adapter = rvAdapProgress

    }


    fun setAdapter(recyclerView: RecyclerView) {
        val list = ArrayList<String>()
        list.add("Cricket")
        list.add("Tennis")
        list.add("Racket")
        list.add("BasketBall")

        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemImgTvBoxBinding>(
            list,
            R.layout.item_img_tv_box, object :
                RecyclerCallback<ItemImgTvBoxBinding, String> {
                override fun bindData(
                    binder: ItemImgTvBoxBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {
                        imgIcon.visibility = View.GONE
                        txtName.text = model
                    }
                }
            })

        recyclerView.adapter = rvAdapProgress

    }

}