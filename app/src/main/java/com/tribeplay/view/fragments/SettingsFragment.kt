package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import com.tribeplay.R
import com.tribeplay.databinding.FragmentSetttingsBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.base.BaseFragment


class SettingsFragment : BaseFragment<FragmentSetttingsBinding>() {


    override fun getLayoutId(): Int {
        return R.layout.fragment_setttings
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarTtl = getString(R.string.sSettings)
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true

        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            tvPayment.setOnClickListener {
                displayItAddStack(SettingspaymentFragment())
            }

            tvReciept.setOnClickListener {
                displayItAddStack(ReciptFragment())
            }

            tvNotifications.setOnClickListener {
                displayItAddStack(SettingsNotificationsFragment())
            }

            tvAccSettings.setOnClickListener {
                displayItAddStack(SettingsAccountFragment())
            }


        }


    }


}