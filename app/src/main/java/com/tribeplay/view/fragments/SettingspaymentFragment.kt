package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import com.tribeplay.R
import com.tribeplay.databinding.FragmentPaymentSettingsBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.base.BaseFragment


class SettingspaymentFragment : BaseFragment<FragmentPaymentSettingsBinding>() {


    override fun getLayoutId(): Int {
        return R.layout.fragment_payment_settings
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarTtl = getString(R.string.manage_payment_option)
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true

        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            tvTransaction.setOnClickListener {
                displayItAddStack(TransactionFragment())
            }

            tvPayment.setOnClickListener {
                displayItAddStack(ManageBankFragment())
            }

        }


    }


}