package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.tribeplay.R
import com.tribeplay.databinding.DialogBottomSheetDelCardBinding
import com.tribeplay.databinding.FragmentManagePaymentBinding
import com.tribeplay.databinding.ItemManageBankBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.adapters.BottomFragmentCallback
import com.tribeplay.view.adapters.RecyclerCallback
import com.tribeplay.view.adapters.RecyclerViewGenricAdapter
import com.tribeplay.view.base.BaseFragment


class ManageBankFragment : BaseFragment<FragmentManagePaymentBinding>() {


    private var bottomSheetDialog: ActionBottomDialogFragment<String, DialogBottomSheetDelCardBinding>? =
        null

    override fun getLayoutId(): Int {
        return R.layout.fragment_manage_payment
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.toolBarTtl = getString(R.string.mMark_interests)
        toolBarModel.backBtn = true
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            recyclerView.layoutManager =
                LinearLayoutManager(context)
            setAdapter()

            underlineText(tvAddBackAcc)

            tvAddBackAcc.setOnClickListener {
                displayItAddStack(
                    AddbankFragment(
                        getString(R.string.add_bank),
                        getString(R.string.add_bank)
                    )
                )
            }

            tvAddCard.setOnClickListener {
                displayItAddStack(
                    AddcardFragment(
                        getString(R.string.add_bank),
                        getString(R.string.add_bank)
                    )
                )
            }
        }


    }


    fun setAdapter() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemManageBankBinding>(
            list,
            R.layout.item_manage_bank, object :
                RecyclerCallback<ItemManageBankBinding, String> {
                override fun bindData(
                    binder: ItemManageBankBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                        tvEdit.setOnClickListener {
                            if (position % 2 == 0)
                                displayItAddStack(
                                    AddcardFragment(
                                        getString(R.string.edit_card_details),
                                        getString(R.string.done)
                                    )
                                )
                            else {
                                displayItAddStack(
                                    AddbankFragment(
                                        getString(R.string.eEdit_Bank_Details),
                                        getString(R.string.done)
                                    )
                                )
                            }
                        }

                        tvRemove.setOnClickListener {

                            dailogCall()
                        }

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


    fun dailogCall() {
        bottomSheetDialog =
            ActionBottomDialogFragment<String, DialogBottomSheetDelCardBinding>(
                R.layout.dialog_bottom_sheet_del_card, object :
                    BottomFragmentCallback<DialogBottomSheetDelCardBinding, String> {
                    override fun bindData(binder: DialogBottomSheetDelCardBinding) {
                        binder.apply {

                            btnCancel.setOnClickListener {
                                bottomSheetDialog?.dismiss()
                            }

                            btnDel.setOnClickListener {
                                bottomSheetDialog?.dismiss()
                            }
                        }
                    }
                })

        bottomSheetDialog?.show(
            homeActivity?.supportFragmentManager!!,
            ActionBottomDialogFragment::class.java.canonicalName
        )
    }


}