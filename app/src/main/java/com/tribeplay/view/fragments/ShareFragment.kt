package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayout
import com.tribeplay.R
import com.tribeplay.databinding.FragmentShareBinding
import com.tribeplay.databinding.ItemInviteBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.adapters.RecyclerCallback
import com.tribeplay.view.adapters.RecyclerViewGenricAdapter
import com.tribeplay.view.base.BaseFragment


class ShareFragment : BaseFragment<FragmentShareBinding>() {


    private var rvAdapProgress: RecyclerViewGenricAdapter<String, ItemInviteBinding>? = null
    private var selectPosition: Int = 0

    override fun getLayoutId(): Int {
        return R.layout.fragment_share
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarTtl = getString(R.string.share_details)
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            recyclerView.layoutManager =
                LinearLayoutManager(context)
            setAdapter()



            if (tabLayout.tabCount <= 0) {
                tabLayout.addTab(tabLayout.newTab().setText(R.string.share_within_platform))
                tabLayout.addTab(tabLayout.newTab().setText(R.string.share_outside_platform))

            }




            tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab?) {
                    selectPosition = tab?.position ?: 0

                    if (selectPosition == 1) {
                        llOutside.visibility = View.VISIBLE
                        llInside.visibility = View.GONE
                    } else {
                        llOutside.visibility = View.GONE
                        llInside.visibility = View.VISIBLE
                    }

                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {

                }

                override fun onTabReselected(tab: TabLayout.Tab?) {

                }

            })


        }


    }


    fun setAdapter() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        rvAdapProgress = RecyclerViewGenricAdapter<String, ItemInviteBinding>(
            list,
            R.layout.item_invite, object :
                RecyclerCallback<ItemInviteBinding, String> {
                override fun bindData(
                    binder: ItemInviteBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {


                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


}