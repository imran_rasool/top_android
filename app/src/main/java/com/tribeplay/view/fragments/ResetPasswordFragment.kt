package com.tribeplay.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.MotionEvent
import android.view.View
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.google.gson.Gson
import com.tribeplay.R
import com.tribeplay.databinding.FragmentResetPassBinding
import com.tribeplay.model.GenericModel
import com.tribeplay.model.ResetPasswordModel
import com.tribeplay.model.ToolBarModel
import com.tribeplay.utils.GlobalConstants
import com.tribeplay.utils.SharedPrefClass
import com.tribeplay.utils.UtilsFunctions
import com.tribeplay.view.base.BaseFragment
import retrofit2.Response

private const val TAG = "ResetPasswordFragment"

class ResetPasswordFragment : BaseFragment<FragmentResetPassBinding>() {

    var isPasswordVisibe = false

    override fun getLayoutId(): Int {
        return R.layout.fragment_reset_pass
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        viewDataBinding?.apply {


            etNewPass.setOnTouchListener(View.OnTouchListener { v, event ->
                val DRAWABLE_RIGHT = 2
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX >= etNewPass.right - etNewPass.compoundDrawables[DRAWABLE_RIGHT].bounds.width()
                    ) {
                        isPasswordVisibe = !isPasswordVisibe

                        if (isPasswordVisibe) {
                            etNewPass.transformationMethod =
                                HideReturnsTransformationMethod.getInstance();
                            etNewPass.setCompoundDrawablesWithIntrinsicBounds(
                                0,
                                0,
                                R.drawable.ic_icon_visibility,
                                0
                            );
                        } else {
                            etNewPass.transformationMethod =
                                PasswordTransformationMethod.getInstance();
                            etNewPass.setCompoundDrawablesWithIntrinsicBounds(
                                0,
                                0,
                                R.drawable.ic_resource_private,
                                0
                            );
                        }

                        return@OnTouchListener true
                    }
                }
                false
            })

            btnReset.setOnClickListener {

                if (isValidate()) {
                    callAPi(
                        ResetPasswordModel(
                            SharedPrefClass().getPrefValue(
                                requireContext(),
                                GlobalConstants.USERID
                            ).toString(), etNewPass.text.toString(), etConfirmPass.text.toString()
                        )
                    )
                }
            }
        }
    }

    private fun callAPi(body: ResetPasswordModel) {
        homeActivity?.let {
            ApiCall.restPasswordApi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    if (mResponse.body()?.status == true) {
                        homeActivity?.clearAllStack()
                        homeActivity?.displayItAddStack(LoginFragment())

                    }

                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    private fun isValidate(): Boolean {
        viewDataBinding?.apply {
            if (etNewPass.text.toString().length < 7) {
                showToast(getString(R.string.valid_pass))
                return false
            } else if (etConfirmPass.text.isNullOrEmpty()) {
                showToast(getString(R.string.enter_confirm_password))
                return false
            } else if (!etNewPass.text.toString()
                    .equals(etConfirmPass.text.toString(), ignoreCase = false)
            ) {
                showToast(getString(R.string.pass_does_not_match))
                return false
            } else {
                return true
            }
        }
        return true
    }

}