package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayout
import com.tribeplay.R
import com.tribeplay.databinding.FragmentChatsBinding
import com.tribeplay.databinding.ItemChatBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.adapters.RecyclerCallback
import com.tribeplay.view.adapters.RecyclerViewGenricAdapter
import com.tribeplay.view.base.BaseFragment


class ChatsFragment : BaseFragment<FragmentChatsBinding>() {


    private var rvAdapProgress: RecyclerViewGenricAdapter<String, ItemChatBinding>? = null
    private var selectPosition: Int = 0

    override fun getLayoutId(): Int {
        return R.layout.fragment_chats
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarTtl = getString(R.string.cChats)
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = false
        toolBarModel.bottomNav = true
        toolBarModel.newChatShow = true
        toolBarModel.notiIconShow = true
        toolBarModel.toolBarProfileShow = true
        toolBarModel.bookMarkShow = true
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)




        viewDataBinding?.apply {
            recyclerView.layoutManager =
                LinearLayoutManager(context)
            setAdapter(0)

            if (tabLayout.tabCount <= 0) {
                tabLayout.addTab(tabLayout.newTab().setText(R.string.iIndividual))
                tabLayout.addTab(tabLayout.newTab().setText(R.string.groups))
                tabLayout.addTab(tabLayout.newTab().setText(R.string.lLeagues))
            }

            etSearch.setOnTouchListener(OnTouchListener { v, event ->
                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX >= etSearch.getRight() - etSearch.getCompoundDrawables()
                            .get(DRAWABLE_RIGHT).getBounds().width()
                    ) {
                        homeActivity?.displayItAddStack(PlayerFilterFragment())
                        //  showToast("ryt")
                        // your action here
                        return@OnTouchListener true
                    } else if (event.rawX >= etSearch.getRight() - etSearch.getCompoundDrawables()
                            .get(DRAWABLE_LEFT).getBounds().width()
                    ) {
                        showToast("left")
                        // your action here
                        return@OnTouchListener true
                    }
                }
                false
            })



            tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab?) {
                    selectPosition = tab?.position ?: 0

                    setAdapter(selectPosition)
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {

                }

                override fun onTabReselected(tab: TabLayout.Tab?) {

                }

            })


        }


    }


    fun setAdapter(i: Int) {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        rvAdapProgress = RecyclerViewGenricAdapter<String, ItemChatBinding>(
            list,
            R.layout.item_chat, object :
                RecyclerCallback<ItemChatBinding, String> {
                override fun bindData(
                    binder: ItemChatBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                        if (i == 2) {
                            llMonth.visibility = View.VISIBLE
                            img.visibility = View.GONE
                        } else {
                            llMonth.visibility = View.GONE
                            img.visibility = View.VISIBLE
                        }

                        linearMain.setOnClickListener {
                            displayItAddStack(MessageFragment(i))
                        }

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


}