package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import com.tribeplay.R
import com.tribeplay.databinding.FragmentGetstartBinding
import com.tribeplay.databinding.FragmentPlayerDetailBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.base.BaseFragment


class PlayerDetailFragment : BaseFragment<FragmentPlayerDetailBinding>() {


    override fun getLayoutId(): Int {
        return R.layout.fragment_player_detail
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarTtl = getString(R.string.pPlayer_details)
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }


}