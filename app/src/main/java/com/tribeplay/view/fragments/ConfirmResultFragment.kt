package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import com.tribeplay.R
import com.tribeplay.databinding.FragmentAddResultBinding
import com.tribeplay.databinding.FragmentConfirmResultBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.base.BaseFragment


class ConfirmResultFragment : BaseFragment<FragmentConfirmResultBinding>() {

    var list = ArrayList<String>()

    override fun getLayoutId(): Int {
        return R.layout.fragment_confirm_result
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarTtl = getString(R.string.cConfirm_The_Result)
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true

        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            btnCall.setOnClickListener {
                popBack()
                popBack()
            }
        }


    }


}