package com.tribeplay.view.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import androidx.core.os.bundleOf
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.google.gson.Gson
import com.orhanobut.hawk.Hawk
import com.tribeplay.MainActivity
import com.tribeplay.R
import com.tribeplay.databinding.FragmentCreateProfile2Binding
import com.tribeplay.model.GenericModel
import com.tribeplay.model.SetLocationModel
import com.tribeplay.model.ToolBarModel
import com.tribeplay.utils.GlobalConstants
import com.tribeplay.utils.GlobalConstants.requestKey
import com.tribeplay.utils.SharedPrefClass
import com.tribeplay.utils.setFragmentResult
import com.tribeplay.utils.widgets.customseekbar.widget.IndicatorSeekBar
import com.tribeplay.utils.widgets.customseekbar.widget.OnSeekChangeListener
import com.tribeplay.utils.widgets.customseekbar.widget.SeekParams
import com.tribeplay.view.base.BaseFragment
import retrofit2.Response


private const val TAG = "CreateProfile2Fragment"

class CreateProfile2Fragment : BaseFragment<FragmentCreateProfile2Binding>() {

    private var radius1 = ""
    private var lat1 = ""
    private var long1 = ""
    private var setAddress = ""

    override fun getLayoutId(): Int {
        return R.layout.fragment_create_profile2
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (!Hawk.isBuilt()) {
            Hawk.init(requireContext()).build()
        }

        viewDataBinding?.apply {


            edtSetLocation.setOnTouchListener(View.OnTouchListener { _, event ->
                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX >= edtSetLocation.right - edtSetLocation.compoundDrawables[DRAWABLE_RIGHT].bounds.width()
                    ) {
                        startActivityForResult(
                            Intent(
                                requireContext(),
                                MainActivity::class.java
                            ), 111
                        )

                        return@OnTouchListener true
                    }
                }
                false
            })

            radius.scale.onSeekChangeListener = object : OnSeekChangeListener {
                override fun onSeeking(seekParams: SeekParams) {
                    //Log.i(TAG, seekParams.seekBar)
                    //Log.i(TAG, seekParams.progress)
                    //Log.i(TAG, seekParams.progressFloat)
                    //Log.i(TAG, seekParams.fromUser)
                    //when tick count > 0
                    //Log.i(TAG, seekParams.thumbPosition)
                    //Log.i(TAG, seekParams.tickText)
                    radius1 = seekParams.progress.toString()
                    Log.d(TAG, "onSeeking: $radius1")
                }

                override fun onStartTrackingTouch(seekBar: IndicatorSeekBar) {
                }

                override fun onStopTrackingTouch(seekBar: IndicatorSeekBar) {

                }

            }

            btnCall.setOnClickListener {

                if (isValidate()) {

                    var userId = SharedPrefClass().getPrefValue(
                        requireContext(),
                        GlobalConstants.USERID
                    ).toString()

                    if (userId == "0" && userId == "") {
                        userId = let { Hawk.get(GlobalConstants.USERID1) ?: "" }
                    }

                    callAPi(
                        SetLocationModel(
                            userId, edtSetLocation.text.toString(),
                            radius1, lat1, long1
                        )
                    )
                }

            }

        }

    }

    private fun callAPi(body: SetLocationModel) {
        homeActivity?.let {
            ApiCall.setLocationApi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    // Log.d(TAG, "onSuccess: ${Gson().toJson(mResponse)}")
                    if (mResponse.body()?.status == true) {
                        setFragmentResult(
                            requestKey,
                            bundleOf(GlobalConstants.FILTER_OBJECT to setAddress)
                        )
                        popBack()
                    }
                }

                override fun onError(msg: String) {
                    showToast("" + msg)
                }
            })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 222) {
            Log.d(TAG, "onActivityResult: $resultCode")
        } else if (requestCode == 111) {
            val address = data?.getStringExtra("key")
            val lat2 = data?.getStringExtra("lat_k")
            val long2 = data?.getStringExtra("long_k")
            if (address != null) {
                setAddress = address
                if (lat2 != null) {
                    lat1 = lat2
                }
                if (long2 != null) {
                    long1 = long2
                }
            }
            viewDataBinding?.edtSetLocation?.setText(address)
        }
    }

    private fun isValidate(): Boolean {
        viewDataBinding?.apply {
            return if (edtSetLocation.text.isNullOrEmpty()) {
                showToast(getString(R.string.add_loc_desc))
                false
            } else {
                true
            }
        }
        return true
    }


}