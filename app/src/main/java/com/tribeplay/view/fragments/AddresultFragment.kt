package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import com.tribeplay.R
import com.tribeplay.databinding.FragmentAddResultBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.base.BaseFragment


class AddresultFragment : BaseFragment<FragmentAddResultBinding>() {

    var list = ArrayList<String>()

    override fun getLayoutId(): Int {
        return R.layout.fragment_add_result
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarTtl = getString(R.string.add_bank_acc_refunds)
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true

        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            btnCall.setOnClickListener {
                displayItAddStack(ConfirmResultFragment())
            }
        }


    }


}