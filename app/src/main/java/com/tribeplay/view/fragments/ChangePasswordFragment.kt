package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import com.tribeplay.R
import com.tribeplay.databinding.FragmentChangePassBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.base.BaseFragment


class ChangePasswordFragment : BaseFragment<FragmentChangePassBinding>() {


    override fun getLayoutId(): Int {
        return R.layout.fragment_change_pass
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarTtl = getString(R.string.cChange_Password)
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        viewDataBinding?.apply {

            btnReset.setOnClickListener {
                popBack()
            }
        }
    }


}