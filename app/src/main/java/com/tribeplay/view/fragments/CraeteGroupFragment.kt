package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.tribeplay.R
import com.tribeplay.databinding.FragmentCreateGroupBinding
import com.tribeplay.databinding.ItemGrpMemeberBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.adapters.RecyclerCallback
import com.tribeplay.view.adapters.RecyclerViewGenricAdapter
import com.tribeplay.view.base.BaseFragment


class CraeteGroupFragment : BaseFragment<FragmentCreateGroupBinding>() {


    private var rvAdapProgress: RecyclerViewGenricAdapter<String, ItemGrpMemeberBinding>? = null

    override fun getLayoutId(): Int {
        return R.layout.fragment_create_group
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarTtl = getString(R.string.create_group)
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            recyclerView.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            setAdapter()

            btnCall.setOnClickListener {
                homeActivity?.clearAllStack()

            }
        }
    }


    fun setAdapter() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        rvAdapProgress = RecyclerViewGenricAdapter<String, ItemGrpMemeberBinding>(
            list,
            R.layout.item_grp_memeber, object :
                RecyclerCallback<ItemGrpMemeberBinding, String> {
                override fun bindData(
                    binder: ItemGrpMemeberBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {


                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


}