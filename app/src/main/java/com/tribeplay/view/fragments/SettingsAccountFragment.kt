package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import com.tribeplay.R
import com.tribeplay.databinding.FragmentSettingsAccountBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.base.BaseFragment


class SettingsAccountFragment : BaseFragment<FragmentSettingsAccountBinding>() {


    override fun getLayoutId(): Int {
        return R.layout.fragment_settings_account
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarTtl = getString(R.string.aAccount_settings)
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true

        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            tvBlock.setOnClickListener {
                displayItAddStack(BlockUserFragment())
            }

            tvInetrestCard.setOnClickListener {
             //   displayItAddStack(CreateProfileFragment(getString(R.string.update_profile)))
            }

            tvPass.setOnClickListener {
                displayItAddStack(ChangePasswordFragment())

            }

        }


    }


}