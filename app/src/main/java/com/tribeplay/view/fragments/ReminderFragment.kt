package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.tribeplay.R
import com.tribeplay.databinding.FragmentPickPlayerBinding
import com.tribeplay.databinding.FragmentReminderBinding
import com.tribeplay.databinding.ItemInviteBinding
import com.tribeplay.databinding.ItemReminderBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.adapters.RecyclerCallback
import com.tribeplay.view.adapters.RecyclerViewGenricAdapter
import com.tribeplay.view.base.BaseFragment


class ReminderFragment : BaseFragment<FragmentReminderBinding>() {


    private var rvAdapProgress: RecyclerViewGenricAdapter<String, ItemReminderBinding>? = null

    override fun getLayoutId(): Int {
        return R.layout.fragment_reminder
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarTtl = getString(R.string.reminder)
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            recyclerView.layoutManager =
                LinearLayoutManager(context)
            setAdapter()



        }
    }


    fun setAdapter() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        rvAdapProgress = RecyclerViewGenricAdapter<String, ItemReminderBinding>(
            list,
            R.layout.item_reminder, object :
                RecyclerCallback<ItemReminderBinding, String> {
                override fun bindData(
                    binder: ItemReminderBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {


                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


}