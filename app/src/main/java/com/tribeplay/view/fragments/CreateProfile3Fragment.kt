package com.tribeplay.view.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.orhanobut.hawk.Hawk
import com.tribeplay.R
import com.tribeplay.databinding.FragmentCreateProfile3Binding
import com.tribeplay.databinding.ItemChipBinding
import com.tribeplay.databinding.ItemChipCrossBinding
import com.tribeplay.model.*
import com.tribeplay.utils.GlobalConstants
import com.tribeplay.utils.SharedPrefClass
import com.tribeplay.view.adapters.RecyclerCallback
import com.tribeplay.view.adapters.RecyclerViewGenricAdapter
import com.tribeplay.view.base.BaseFragment
import retrofit2.Response

private const val TAG = "CreateProfile3Fragment"

class CreateProfile3Fragment : BaseFragment<FragmentCreateProfile3Binding>() {

    private var rvAdapCategory: RecyclerViewGenricAdapter<SportCategoryModel, ItemChipBinding>? =
        null
    private var rvAdapSubCategory: RecyclerViewGenricAdapter<SportSubCategory, ItemChipCrossBinding>? =
        null
    private val topCategoryList = ArrayList<SportCategoryModel>()
    private var topCat = ""
    private var subCat = ""
    private var userId: String = ""
    private var listSubId = ArrayList<String>()


    override fun getLayoutId(): Int {
        return R.layout.fragment_create_profile3
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.toolBarTtl = getString(R.string.select_sports)
        toolBarModel.backBtn = true
        return toolBarModel
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (!Hawk.isBuilt()) {
            Hawk.init(requireContext()).build()
        }

        userId = SharedPrefClass().getPrefValue(requireContext(), GlobalConstants.USERID).toString()
        if (userId == "0" && userId == "") {
            userId = let { Hawk.get(GlobalConstants.USERID1) ?: "" }
        }

        viewDataBinding?.apply {

            tvEmail.setOnClickListener {
                val recipient = "info@appsums.com"
                val subject = "Request"
                val body = "Add Sport Category"
                sendEmail(recipient, subject, body)
            }

            btnCall.setOnClickListener {

                subCat = android.text.TextUtils.join(",", listSubId);
                if (subCat != "") {
                    callAPi2(SelectSportModel(userId, topCat, subCat))
                    //Log.d(TAG, "onViewCreated: $subCat$topCat$userId")
                } else {
                    showToast("Select Sub Category")
                }
            }

            val layoutManager = FlexboxLayoutManager(context)
            layoutManager.flexDirection = FlexDirection.ROW
            layoutManager.justifyContent = JustifyContent.CENTER
            recyclerView.layoutManager = layoutManager
            setAdapter()

        }

        callCategoryApi()
        setWheelList()


    }

    private fun sendEmail(recipient: String, subject: String, message: String) {
        val mIntent = Intent(Intent.ACTION_SEND)
        mIntent.data = Uri.parse("mailto:")
        mIntent.type = "text/plain"
        mIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(recipient))
        mIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        mIntent.putExtra(Intent.EXTRA_TEXT, message)

        try {
            startActivity(Intent.createChooser(mIntent, "Choose Email Client..."))
        } catch (e: Exception) {
            showToast(e.message)
        }

    }


    private fun setWheelList() {
        val list = ArrayList<String>()
        list.add("Cricket")
        list.add("Tennis")
        list.add("Racket")
        list.add("BasketBall")

        viewDataBinding?.layoutWheel?.apply {
            mainWheelCenter.data = list
            mainWheelCenter.setOnItemSelectedListener { picker, data, position ->
                showToast("asdsdf" + position + "" + data)
            }
        }
    }

    private var mPosition: Int = -1
    private fun setAdapter() {
        rvAdapCategory = RecyclerViewGenricAdapter<SportCategoryModel, ItemChipBinding>(
            topCategoryList,
            R.layout.item_chip, object :
                RecyclerCallback<ItemChipBinding, SportCategoryModel> {
                override fun bindData(
                    binder: ItemChipBinding,
                    model: SportCategoryModel,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {
                        txtName.text = model.category_name
                        topCat = "" + model.tbl_sport_category_id
                        // subCategoryList.addAll(topCategoryList[0].Sport_sub_category)

                        if (mPosition == position) {

                            cardViewItemCategory.setCardBackgroundColor(
                                ContextCompat.getColor(
                                    requireContext(),
                                    R.color.colorGreen
                                )
                            )
                            txtName.setTextColor(
                                ContextCompat.getColor(
                                    requireContext(),
                                    R.color.white
                                )
                            )

                        } else {
                            cardViewItemCategory.setCardBackgroundColor(
                                ContextCompat.getColor(
                                    requireContext(),
                                    R.color.colorGrayBtn
                                )
                            )
                            txtName.setTextColor(
                                ContextCompat.getColor(
                                    requireContext(),
                                    R.color.black
                                )
                            )

                        }

                        cardViewItemCategory.setOnClickListener {

                            topCat = "" + model.tbl_sport_category_id
                            mPosition = position
                            rvAdapCategory?.notifyDataSetChanged()

                            listSubId.clear()

                            viewDataBinding?.apply {
                                val layoutManager1 = FlexboxLayoutManager(context)
                                layoutManager1.flexDirection = FlexDirection.ROW
                                layoutManager1.justifyContent = JustifyContent.CENTER
                                recyclerView1.layoutManager = layoutManager1
                                val subCategoryList = ArrayList<SportSubCategory>()
                                subCategoryList.addAll(topCategoryList[position].Sport_sub_category)

                                setAdapter1(subCategoryList)

                            }

                        }
                    }
                }
            })

        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapCategory

        }
    }


    private fun setAdapter1(subCategoryList: ArrayList<SportSubCategory>) {

        rvAdapSubCategory = RecyclerViewGenricAdapter<SportSubCategory, ItemChipCrossBinding>(
            subCategoryList,
            R.layout.item_chip_cross, object :
                RecyclerCallback<ItemChipCrossBinding, SportSubCategory> {
                override fun bindData(
                    binder: ItemChipCrossBinding,
                    model: SportSubCategory,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {
                        txtName.text = model.sub_category_name

                        if (model.isSelect) {

                            llSubItem.background = ContextCompat.getDrawable(
                                requireContext(),
                                R.drawable.round_solid_green_14_btn
                            )

                            txtName.setTextColor(
                                ContextCompat.getColor(
                                    requireContext(),
                                    R.color.white
                                )
                            )

                        } else {

                            llSubItem.background = ContextCompat.getDrawable(
                                requireContext(),
                                R.drawable.outline_green_bg
                            )

                            txtName.setTextColor(
                                ContextCompat.getColor(
                                    requireContext(),
                                    R.color.black
                                )
                            )
                        }
                        llSubItem.setOnClickListener {

                            listSubId.add(model.tbl_sport_sub_category_id.toString())
                            subCategoryList[position].isSelect = !subCategoryList[position].isSelect

                            rvAdapSubCategory?.notifyDataSetChanged()

                        }
                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView1.adapter = rvAdapSubCategory
        }
    }

    private fun callCategoryApi() {
        homeActivity?.let {
            ApiCall.sportCategoryApi(it, object : ResponseListener<GenericModel1> {
                override fun onSuccess(mResponse: Response<GenericModel1>) {
                    topCategoryList.clear()
                    mResponse.body()?.data?.let { it1 -> topCategoryList.addAll(it1) }
                    rvAdapCategory?.notifyDataSetChanged()

                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    private fun callAPi2(body: SelectSportModel) {
        homeActivity?.let {
            ApiCall.selectCategoryApi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    //  Log.d(TAG, "onSuccess: ${Gson().toJson(mResponse.body()?.message)}")
                    if (mResponse.body()?.status == true) {
                        listSubId.clear()
                        homeActivity?.displayItAddStack(CreateProfile4Fragment())
                    }
                }

                override fun onError(msg: String) {

                }
            })
        }
    }


}