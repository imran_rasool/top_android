package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.tribeplay.R
import com.tribeplay.databinding.FragmentPickPlayerBinding
import com.tribeplay.databinding.ItemInviteBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.adapters.RecyclerCallback
import com.tribeplay.view.adapters.RecyclerViewGenricAdapter
import com.tribeplay.view.base.BaseFragment


class AddmemberFragment : BaseFragment<FragmentPickPlayerBinding>() {


    private var rvAdapProgress: RecyclerViewGenricAdapter<String, ItemInviteBinding>? = null

    override fun getLayoutId(): Int {
        return R.layout.fragment_pick_player
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarTtl = getString(R.string.add_mem)
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            btnCall.setText(R.string._add)
            recyclerView.layoutManager =
                LinearLayoutManager(context)
            setAdapter()

            btnCall.setOnClickListener {
popBack()
            }

        }
    }


    fun setAdapter() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        rvAdapProgress = RecyclerViewGenricAdapter<String, ItemInviteBinding>(
            list,
            R.layout.item_invite, object :
                RecyclerCallback<ItemInviteBinding, String> {
                override fun bindData(
                    binder: ItemInviteBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {


                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


}