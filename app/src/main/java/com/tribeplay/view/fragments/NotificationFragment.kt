package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.tribeplay.R
import com.tribeplay.databinding.FragmentNotificationsBinding
import com.tribeplay.databinding.ItemNotiBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.adapters.NotificationoAdapter1
import com.tribeplay.view.adapters.RecyclerCallback
import com.tribeplay.view.adapters.ViewBinderHelper
import com.tribeplay.view.base.BaseFragment


class NotificationFragment : BaseFragment<FragmentNotificationsBinding>() {


    private var viewBinderHelper: ViewBinderHelper? = null
    private var rvAdapProgress: NotificationoAdapter1<String, ItemNotiBinding>? = null

    override fun getLayoutId(): Int {
        return R.layout.fragment_notifications
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.toolBarTtl = getString(R.string.nNotification)

        toolBarModel.backBtn = true
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            recyclerView.layoutManager =
                LinearLayoutManager(context)

        }
        setAdapter()
    }


    fun setAdapter() {
        viewBinderHelper = ViewBinderHelper()
        viewBinderHelper?.setOpenOnlyOne(true)

        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        rvAdapProgress = NotificationoAdapter1<String, ItemNotiBinding>(
            list,
            R.layout.item_noti, object :
                RecyclerCallback<ItemNotiBinding, String> {
                override fun bindData(
                    binder: ItemNotiBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {
                    viewBinderHelper?.bind(binder.swipeRevealLayout, position.toString());

                    binder.apply {
                        frameDel.setOnClickListener {
                            list.removeAt(position)
                            rvAdapProgress?.notifyDataSetChanged()
                        }

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (rvAdapProgress != null) {
            saveStates(outState)
        }
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (rvAdapProgress != null) {
            restoreStates(savedInstanceState)
        }
    }


    fun saveStates(outState: Bundle?) {
        viewBinderHelper?.saveStates(outState)
    }

    fun restoreStates(inState: Bundle?) {
        viewBinderHelper?.restoreStates(inState)
    }
}

