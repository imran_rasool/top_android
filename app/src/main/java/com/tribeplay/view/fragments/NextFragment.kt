package com.tribeplay.view.fragments

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.tribeplay.R
import com.tribeplay.databinding.FragmentCreateProfileBinding
import com.tribeplay.databinding.FragmentNextBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.adapters.HorizontalPagerAdapter
import com.tribeplay.view.base.BaseFragment


class NextFragment : BaseFragment<FragmentNextBinding>() {

    private var dotscount = 0
    private var horizontalPagerAdapter: HorizontalPagerAdapter? = null

    override fun getLayoutId(): Int {
        return R.layout.fragment_next
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            horizontalPagerAdapter = HorizontalPagerAdapter(
                requireContext(),
                ArrayList(),
                object : HorizontalPagerAdapter.StartListener {
                    override fun onStartClick(position: Int) {
                        if (position < 2) {
                            viewPager?.setCurrentItem(position + 1)
                        } else {
                            homeActivity?.displayItNoStack(LoginFragment())
                        }
                    }
                })

            viewPager?.adapter = horizontalPagerAdapter
            dotscount = horizontalPagerAdapter?.count!!
            var dots = arrayOfNulls<ImageView>(dotscount)

            sliderDotspanel?.removeAllViews()
            for (i in 0 until dotscount) {
                dots[i] = ImageView(context)
                dots[i]!!.setImageDrawable(
                    context?.let {
                        ContextCompat.getDrawable(
                            it,
                            R.drawable.round_solid_white_btn
                        )
                    })
                val params = LinearLayout.LayoutParams(
                    100,
                    24
                )
                params.setMargins(8, 0, 8, 0)
                sliderDotspanel!!.addView(dots[i], params)
            }
            dots[0]?.setImageDrawable(
                context?.let {
                    ContextCompat.getDrawable(
                        it, R.drawable.round_solid_green_14_btn
                    )
                }
            )



            viewPager?.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrollStateChanged(state: Int) {
                }

                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {
                }

                override fun onPageSelected(position: Int) {
                    for (i in 0 until dotscount) {
                        dots[i]?.setImageDrawable(
                            context?.let {
                                ContextCompat.getDrawable(
                                    it, R.drawable.round_solid_white_btn
                                )
                            }
                        )
                    }
                    dots[position]?.setImageDrawable(
                        context?.let {
                            ContextCompat.getDrawable(
                                it, R.drawable.round_solid_green_14_btn
                            )
                        }
                    )
                }

            })


        }

    }


}