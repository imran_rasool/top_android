package com.tribeplay.view.base

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import com.tribeplay.R
import com.tribeplay.databinding.DialogBottomSheetSelectSportsBinding
import com.tribeplay.model.ParamModel
import com.tribeplay.view.activities.HomeActivity


class CustomDialog(val mContainer: HomeActivity) {

    var dialog: Dialog? = null

    private fun initDialog(layout: View): Dialog {
        if (dialog != null) {
            dialog!!.dismiss()
            dialog = null
        }
        dialog = CustomDialog(mContainer, R.style.mytheme)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(true)
        dialog!!.setContentView(layout)
        return dialog!!
    }

    class CustomDialog(context: Context, themeId: Int) : Dialog(context, themeId) {
        override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
            currentFocus?.let {
                val imm: InputMethodManager = context.applicationContext.getSystemService(
                    Context.INPUT_METHOD_SERVICE
                ) as (InputMethodManager)
                imm.hideSoftInputFromWindow(it.windowToken, 0)
            }
            return super.dispatchTouchEvent(ev)
        }
    }

    fun selectSportzDialog(): Pair<DialogBottomSheetSelectSportsBinding, Dialog> {
        val binding = DataBindingUtil
            .inflate<DialogBottomSheetSelectSportsBinding>(
                LayoutInflater.from(mContainer),
                R.layout.dialog_bottom_sheet_select_sports,
                null,
                false
            )

        val dialog = initDialog(binding.root)
        dialog.setCancelable(false)
        dialog.show()
        return Pair(binding, dialog)
    }


    interface DialogListener {
        fun positiveBtn()
        fun positiveBtn(paramModel: ParamModel)
    }


}