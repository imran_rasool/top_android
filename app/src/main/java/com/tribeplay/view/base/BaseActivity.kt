package com.tribeplay.view.base

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.tribeplay.R
import com.tribeplay.application.TribesPlayApplication
import com.tribeplay.utils.GlobalConstants
import com.tribeplay.utils.SharedPrefClass
import com.tribeplay.view.activities.HomeActivity
import java.util.*

abstract class BaseActivity<T : ViewDataBinding> : AppCompatActivity() {

    var viewDataBinding: T? = null
    private var reqCode: Int = 0
    private var progressDialog: Dialog? = null
    private var mFragmentManager: FragmentManager? = null
    var _serverTime = ""
    var currentFragment: Fragment? = null


    @LayoutRes
    abstract fun getLayoutId(): Int

    abstract fun setContainerLayout(): Int

    fun getviewDataBinding(): T {
        return viewDataBinding!!
    }

//    fun getProfileData(): LoginData? {
//        val jsonObj =
//            SharedPrefClass().getPrefValue(TribesPlayApplication.instance, GlobalConstants.USER_DATA)
//                .toString()
//        val json = if (jsonObj == "") null else Gson().fromJson(
//            jsonObj,
//            LoginData::class.java
//        )
//        return json
//    }


    fun hideStatausBar() {
        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
    }

    private fun performDataBinding() {
        viewDataBinding = DataBindingUtil.setContentView<T>(this, getLayoutId())
    }

    fun clearAllStack() {
        supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    fun displayItAddStack(mFragment: Fragment, bundle: Bundle? = null) {
        if (bundle != null) {
            mFragment.arguments = bundle
        }
        currentFragment = mFragment
        val fragmentTransaction = supportFragmentManager
            .beginTransaction()

        // if (isBack) {
        fragmentTransaction.addToBackStack(mFragment::class.java.canonicalName)
        //}

        fragmentTransaction.replace(
            setContainerLayout(),
            currentFragment!!,
            mFragment::class.java.canonicalName
        )
            .commitAllowingStateLoss()
    }


    fun displayItAddStack(mFragment: Fragment, bundle: Bundle? = null, conatinerId: Int?) {
        if (bundle != null) {
            mFragment.arguments = bundle
        }
        currentFragment = mFragment
        val fragmentTransaction = supportFragmentManager
            .beginTransaction()

        // if (isBack) {
        fragmentTransaction.addToBackStack(mFragment::class.java.canonicalName)
        //}

        fragmentTransaction.replace(
            conatinerId!!,
            currentFragment!!,
            mFragment::class.java.canonicalName
        )
            .commitAllowingStateLoss()
    }

    fun popBack() {
        supportFragmentManager.popBackStack()
    }

    fun displayItNoStack(mFragment: Fragment, bundle: Bundle? = null) {
        if (bundle != null) {
            mFragment.arguments = bundle
        }
        currentFragment = mFragment
        val fragmentTransaction = supportFragmentManager
            .beginTransaction()

        // if (isBack) {
        //  fragmentTransaction.addToBackStack(mFragment::class.java.canonicalName)
        //}

        fragmentTransaction.replace(
            setContainerLayout(),
            currentFragment!!,
            mFragment::class.java.canonicalName
        )
            .commitAllowingStateLoss()
    }


    fun getCurFragment(): Fragment {
        return currentFragment!!
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Places.initialize(this, getString(R.string.google_maps_key))
        performDataBinding()
        initializeProgressDialog()
    }

    fun callHomeActivity() {
        finish()
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        val view = currentFocus
        if (view != null && (ev.action == MotionEvent.ACTION_UP || ev.action == MotionEvent.ACTION_MOVE) && view is EditText && !view.javaClass.name.startsWith(
                "android.webkit."
            )
        ) {
            val scrcoords = IntArray(2)
            view.getLocationOnScreen(scrcoords)
            val x = ev.rawX + view.left - scrcoords[0]
            val y = ev.rawY + view.top - scrcoords[1]
            if (x < view.left || x > view.right || y < view.top || y > view.bottom) {

                // if (currentFragment !is ChatFragment)
                (Objects.requireNonNull(this.getSystemService(Context.INPUT_METHOD_SERVICE)) as InputMethodManager).hideSoftInputFromWindow(
                    this.window.decorView.applicationWindowToken, 0
                )
            }
        }
        return super.dispatchTouchEvent(ev)
    }

    fun showToast(msg: String?) {
        try {
            if (!msg.isNullOrEmpty())
                Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {
        }
    }

    fun showToast(msg: Int) {
        try {
            Toast.makeText(this, getString(msg), Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {

        }
    }


    private fun initializeProgressDialog() {
        progressDialog = Dialog(this, R.style.transparent_dialog_borderless)
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(this),
            R.layout.progress_dialog,
            null,
            false
        )
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setContentView(binding.root)
        mFragmentManager = supportFragmentManager
        progressDialog!!.setCancelable(false)
    }

    /*
    * Method to start progress dialog*/
    fun startProgressDialog() {
        if (progressDialog != null && !progressDialog!!.isShowing) {
            try {
                progressDialog!!.show()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    /*
     * Method to stop progress dialog*/
    fun stopProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            try {
                progressDialog!!.dismiss()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun isLogin(): Boolean {
        val userId =
            SharedPrefClass().getPrefValue(TribesPlayApplication.instance, GlobalConstants.USERID)
        return userId != null
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            if (requestCode == 321) {
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        checkFragment()!!.onActivityResult(requestCode, resultCode, data)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
//                else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
//                    val status = Autocomplete.getStatusFromIntent(data)
//                    Log.e("HOME", "Stuck in error$status")
//                }
                else if (resultCode == Activity.RESULT_CANCELED) {
                    Log.e("HOME", "Intent Cancelled")
                }
            }
        }
    }

    private fun checkFragment(): Fragment? {
        return supportFragmentManager.findFragmentById(setContainerLayout())
    }

    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val nw = connectivityManager.activeNetwork ?: return false
            val actNw = connectivityManager.getNetworkCapabilities(nw) ?: return false
            return when {
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            val nwInfo = connectivityManager.activeNetworkInfo ?: return false
            return nwInfo.isConnected
        }
    }

    private var mDialogLoader: Dialog? = null

    fun showLoader() {
        hideLoader()
        mDialogLoader = setLoadingDialog(this);
    }

    fun hideLoader() {
        if (mDialogLoader != null && mDialogLoader!!.isShowing) {
            mDialogLoader?.cancel()
        }
    }

    fun setLoadingDialog(activity: AppCompatActivity): Dialog {

        val dialog = Dialog(activity)
        dialog.show()
        if (dialog.window != null) {
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        dialog.setContentView(R.layout.loader)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        return dialog
    }


    fun logout() {
        showToast(getString(R.string.logout_session))
        val notificationToken =
            SharedPrefClass().getPrefValue(this, GlobalConstants.DEVICE_ID).toString()

        SharedPrefClass().clearAll(this)
        SharedPrefClass().putObject(this, GlobalConstants.DEVICE_ID, notificationToken)
        startActivity(
            Intent(
                this,
                HomeActivity::class.java
            ).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        )
        finishAffinity()
    }


}