package com.tribeplay.view.base

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.RoundRectShape
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.text.Html
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.text.SpannableString
import android.text.Spanned
import android.text.style.StrikethroughSpan
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.tribeplay.R
import com.tribeplay.databinding.TakePictureDialogBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.utils.TakePictureUtils
import com.tribeplay.view.activities.HomeActivity
import com.tribeplay.view.listener.FragmentView
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


abstract class BaseFragment<T : ViewDataBinding> : Fragment(), FragmentView {

    private var timedialog: Dialog? = null
    var baseActivity: BaseActivity<ViewDataBinding>? = null
    var homeActivity: HomeActivity? = null
    var viewDataBinding: T? = null
    var mRootView: View? = null
    val passwordPattern = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$"
    val patternPassword = Pattern.compile(passwordPattern)
    val format = SimpleDateFormat("dd MMMM yyyy", Locale("en"))
    val format3 = SimpleDateFormat("MMMM dd,yyyy", Locale("en"))
    val AUTOCOMPLETE_REQUEST_CODE = 102
    var lat = "28.6273"
    var long = "77.3725"

    enum class BUNDLECONSTANTS {
        TITLE, WEBURL
    }

    @LayoutRes
    abstract fun getLayoutId(): Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (viewDataBinding == null) {
            viewDataBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
            mRootView = viewDataBinding!!.root
        }
        return mRootView
    }

    fun hideStatusBar() {
        homeActivity?.hideStatausBar()
    }

    fun underlineText(textview: TextView) {
        textview.paintFlags = textview.paintFlags or Paint.UNDERLINE_TEXT_FLAG
    }

    fun stringToFloat(value: String?): Float {
        return value?.toFloat() ?: 0f
    }

    fun stringToInt(value: String?): Int {
        return value?.toInt() ?: 0
    }

    override fun onStart() {
        super.onStart()
        homeActivity?.setToolbarText(getToolBar() ?: ToolBarModel())
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        homeActivity = context as HomeActivity
        baseActivity = context as BaseActivity<ViewDataBinding>
    }

    fun clrNotification() {
        val nMgr =
            requireActivity().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        nMgr.cancelAll()
    }

    fun setHtmlText(text: String): Spanned? {
        return Html.fromHtml(text)
    }

    public fun setImageWithUri(
        uri: File?,
        imgProfile: ImageView?
    ) {
        Glide.with(baseActivity!!)
            .load(uri)
            .placeholder(R.drawable.ic_usericon)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)
            .into(imgProfile!!)
    }

    public fun setImageWithUrl(
        uri: String?,
        imgProfile: ImageView?
    ) {
        Glide.with(baseActivity!!)
            .load(uri)
            .placeholder(R.drawable.ic_usericon)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .override(200, 200)
            .skipMemoryCache(true)
            .into(imgProfile!!)
    }

    fun setImgTint(img: ImageView, color: Int) {
        ImageViewCompat.setImageTintList(
            img,
            ColorStateList.valueOf(setColor(color))
        )
    }

    fun setColor(colorId: Int): Int {
        return ContextCompat.getColor(requireContext(), colorId)
    }

    fun setDrawable(id: Int): Drawable? {
        return ContextCompat.getDrawable(requireContext(), id)
    }

    fun popBack() {
        homeActivity?.supportFragmentManager?.popBackStack()
    }

    fun getColor(colorId: Int): Int {
        return ContextCompat.getColor(requireActivity(), colorId)
    }

    fun getStringText(id: Int): String {
        return getString(id)
    }

    fun showToast(msg: String?) {
        homeActivity?.showToast(msg)
    }

    fun showToast(msg: Int) {
        homeActivity?.showToast(msg)
    }


    fun createShapeDrawable() {
        var r = 8;
        var shape =
            ShapeDrawable(RoundRectShape(floatArrayOf(0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f), null, null));
        shape.getPaint().setColor(Color.RED);
        //   view.setBackground(shape);
    }

    fun disableEMO(editText: EditText) {
        val EMOJI_FILTER = InputFilter { source, start, end, dest, dstart, dend ->
            for (index in start until end) {
                val type = Character.getType(source.get(index))
                if (type == Character.SURROGATE.toInt() || source == "☺") {
                    return@InputFilter ""
                }
            }
            null
        }
        editText.filters = arrayOf(EMOJI_FILTER)
    }

    fun disableEmojiLen(editText: EditText, len: Int) {
        val SPACE_FILTER = InputFilter { source, start, end, dest, dstart, dend ->
            for (index in start until end) {
                val type = Character.getType(source[index])
                if (type == Character.SURROGATE.toInt() || source == "☺") {
                    return@InputFilter ""
                }
            }
            null
        }
        editText.filters = arrayOf(SPACE_FILTER, LengthFilter(len))
    }

    private fun openSettings(activity: Activity) {

        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", activity.packageName, null)
        intent.data = uri
        activity.startActivityForResult(intent, 101)
    }

    fun displayItAddStack(mFragment: Fragment, bundle: Bundle? = null) {
        if (bundle != null) {
            mFragment.arguments = bundle
        }
        homeActivity?.displayItAddStack(mFragment)
    }


    fun displayItAddStack(mFragment: Fragment, bundle: Bundle? = null, conatinerId: Int?) {
        if (bundle != null) {
            mFragment.arguments = bundle
        }
        //  currentFragment = mFragment
        val fragmentTransaction = childFragmentManager
            .beginTransaction()

        // if (isBack) {
        fragmentTransaction.addToBackStack(mFragment::class.java.canonicalName)
        //}

        fragmentTransaction.replace(
            conatinerId!!,
            mFragment!!,
            mFragment::class.java.canonicalName
        )
            .commitAllowingStateLoss()
    }

    fun getDynamicText(id: Int, dynamicText: String?): String {
        return String.format(
            getString(id), dynamicText
        )
    }


    fun strikeText(content2: String, start: Int, end: Int): SpannableString {
        val spannableString2 = SpannableString(content2)
        spannableString2.setSpan(StrikethroughSpan(), start, end, 0)
        return spannableString2
    }

    fun strikeText(content2: String): SpannableString {
        val content = String.format(
            "", content2
        )
        val spannableString2 = SpannableString(content)
        spannableString2.setSpan(StrikethroughSpan(), 0, content.length, 0)
        return spannableString2
    }


    fun getRealPathFromURI(contentURI: Uri): String? {
        val result: String?
        val cursor = requireActivity().contentResolver.query(contentURI, null, null, null, null)
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.path
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            result = cursor.getString(idx)
            cursor.close()
        }
        return result
    }

    fun getCount(textItemCount: AppCompatTextView): Int {
        var count: Int =
            textItemCount.text.trim().toString().toInt() + 1
        return count
    }

    fun getCountMinus(textItemCount: AppCompatTextView): Int {
        var count: Int =
            textItemCount.text.trim().toString().toInt() - 1
        return if (count >= 0)
            count
        else 1
    }


    fun isLogin(): Boolean {
        return homeActivity?.isLogin() ?: false
    }


    fun changeDateFormat(startDate: String?): String {
        try {
            // "2021-03-11 18:53:38"
            var date = startDate
            var spf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val newDate: Date = spf.parse(date)
            spf = SimpleDateFormat("dd-MM-yyyy hh:mm a")
            return spf.format(newDate)
        } catch (e: Exception) {
            return startDate!!
        }
    }


    fun datePicker(eText: EditText) {
        val cldr = Calendar.getInstance()
        val day = cldr[Calendar.DAY_OF_MONTH]
        val month = cldr[Calendar.MONTH]
        val year = cldr[Calendar.YEAR]
        // date picker dialog
        // date picker dialog
        val picker = DatePickerDialog(
            requireContext(), { view, year, monthOfYear, dayOfMonth ->

                eText.setText(dayOfMonth.toString() + "/" + (monthOfYear + 1) + "/" + year)
            },
            year,
            month,
            day
        )
        picker.datePicker.maxDate = System.currentTimeMillis()
        picker.show()
    }

    open fun showImageDailoge(context: Activity, tempImage: String?, isDocument: Boolean) {
        val takePictureDailogBinding: TakePictureDialogBinding?
        val builder = AlertDialog.Builder(
            requireContext()
        )
        //layout inflate custom
        val dialogView: View =
            LayoutInflater.from(context).inflate(R.layout.take_picture_dialog, null)
        builder.setView(dialogView)
        takePictureDailogBinding = DataBindingUtil.bind(dialogView)
        val imagedialog = builder.create()
        takePictureDailogBinding?.apply {
            takePictureDailogBinding.tvCamera.setOnClickListener(View.OnClickListener {
                imagedialog.dismiss()
                TakePictureUtils.takePicture(context, tempImage)
            })
            takePictureDailogBinding.tvGallery.setOnClickListener(View.OnClickListener {
                imagedialog.dismiss()
                TakePictureUtils.openGallery(context)
            })
            takePictureDailogBinding.tvCancel.setOnClickListener(View.OnClickListener { imagedialog.dismiss() })
            takePictureDailogBinding.tvDocuments.setOnClickListener(View.OnClickListener {
                imagedialog.dismiss()
                TakePictureUtils.openPdf(context)
            })
            if (isDocument) {
                takePictureDailogBinding.tvCamera.setVisibility(View.GONE)
                takePictureDailogBinding.tvGallery.setVisibility(View.GONE)
                takePictureDailogBinding.view.setVisibility(View.GONE)
                takePictureDailogBinding.isdocView.setVisibility(View.GONE)
                takePictureDailogBinding.tvDocuments.setVisibility(View.VISIBLE)
                takePictureDailogBinding.tvCancel.setVisibility(View.VISIBLE)
            } else {
                takePictureDailogBinding.tvCamera.setVisibility(View.VISIBLE)
                takePictureDailogBinding.tvGallery.setVisibility(View.VISIBLE)
                takePictureDailogBinding.view.setVisibility(View.VISIBLE)
                takePictureDailogBinding.isdocView.setVisibility(View.GONE)
                takePictureDailogBinding.tvDocuments.setVisibility(View.GONE)
                takePictureDailogBinding.tvCancel.setVisibility(View.VISIBLE)
            }
            builder.setCancelable(false)
            imagedialog.setCancelable(false)
            imagedialog.show()
            imagedialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            imagedialog.window!!.setGravity(Gravity.BOTTOM)
        }

    }

}