package com.tribeplay.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.viewpager.widget.PagerAdapter
import com.tribeplay.view.activities.HomeActivity

class GenericTabPagerAdapter<T, VM : ViewDataBinding>(
    val activity: HomeActivity,
    val layoutID: Int,
    val items: List<T>,
    val bindingInterface: TabPagerCallback<VM>
) : PagerAdapter() {

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as View
    }


    override fun getCount(): Int {
        return items.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return bindingInterface.setTitle(position)
    }

    override fun getItemPosition(`object`: Any): Int {
        return POSITION_NONE
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = LayoutInflater.from(activity).inflate(layoutID, container, false)
        val viewDataBinding = DataBindingUtil.bind<VM>(itemView)

        bindingInterface.bindData(viewDataBinding!!, position)
        container.addView(itemView)

        return itemView
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}