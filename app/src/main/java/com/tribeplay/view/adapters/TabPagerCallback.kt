package com.tribeplay.view.adapters

import androidx.databinding.ViewDataBinding

interface TabPagerCallback<VM : ViewDataBinding> {
    fun bindData(binder: VM?, position: Int)

    fun setTitle(position: Int): String
}

