package com.tribeplay.view.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import com.tribeplay.R


class HorizontalPagerAdapter(
    var context: Context,
    imgList: ArrayList<String>,
    var listener: StartListener
) : PagerAdapter() {

    var drawables = intArrayOf(
        R.drawable.img2,
        R.drawable.img2,
        R.drawable.img2
    )


    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        var view = LayoutInflater.from(context).inflate(R.layout.intro_pager_row, container, false)

        var desc: TextView = view?.findViewById(R.id.tv3)!!
        var btnCall: Button = view?.findViewById(R.id.btnCall)!!
        var tvSkip: TextView = view?.findViewById(R.id.tvSkip)!!

        var model = getList()[position]

        desc.setText(model.desc)


//        if (position == 2) {
//            listener?.onStartClick()
//        }

        btnCall.setOnClickListener {
            //  if (position == 2)
            listener?.onStartClick(position)

        }

        tvSkip.setOnClickListener {
            listener?.onStartClick(4)
        }


//
//        getStarted?.setOnClickListener {
//            listener?.onStartClick()
//        }

        container.addView(view)
        return view
    }


    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return 3
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val view = `object` as View
        container.removeView(view)
    }

    interface StartListener {
        fun onStartClick(position: Int)
    }

    data class ModelIntro(var title1: String, var desc: String, var img1: Int)

    fun getList(): ArrayList<ModelIntro> {
        var list = ArrayList<ModelIntro>()
        list.add(
            ModelIntro(
                "Get a new look",
                "Pick your sports and activities. Mark the map. Find others. Play.",
                R.drawable.img2
            )
        )
        list.add(
            ModelIntro(
                "Book your services at your favorite salon",
                "Sports close to home and around the world for you to play in.",
                R.drawable.img2
            )
        )
        list.add(
            ModelIntro(
                "100% safe and secure",
                "Set up individual activities and build groups in your sports in your area.",
                R.drawable.img2
            )
        )

        return list

    }

}