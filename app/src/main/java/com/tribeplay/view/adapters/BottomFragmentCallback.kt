package com.tribeplay.view.adapters

import android.view.View
import androidx.databinding.ViewDataBinding

interface BottomFragmentCallback<VM : ViewDataBinding, T> {
    fun bindData(binder: VM)

}
