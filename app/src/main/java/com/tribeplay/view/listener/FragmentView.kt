package com.tribeplay.view.listener

import com.tribeplay.model.ToolBarModel


interface FragmentView {
    fun getToolBar(): ToolBarModel?
}