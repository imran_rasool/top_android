package com.tribeplay.view.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.tribeplay.R
import kotlinx.coroutines.*

class SpalshActivity : AppCompatActivity() {

    private val activityScope = CoroutineScope(Dispatchers.Main)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
        setContentView(R.layout.activity_splash)

        activityScope.launch {
            delay(2000)
            startActivity(Intent(this@SpalshActivity, HomeActivity::class.java))
            finish()
        }



    }

    override fun onPause() {
        super.onPause()
        activityScope.cancel()
    }

}