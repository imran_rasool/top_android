package com.tribeplay.view.activities

import android.os.Bundle
import android.view.View
import com.tribeplay.R
import com.tribeplay.databinding.ActivityHomeBinding
import com.tribeplay.databinding.DialogBottomSheetLocBinding
import com.tribeplay.model.ToolBarModel
import com.tribeplay.view.adapters.BottomFragmentCallback
import com.tribeplay.view.base.BaseActivity
import com.tribeplay.view.fragments.*

open class HomeActivity : BaseActivity<ActivityHomeBinding>() {


    private var bottomSheetDialog: ActionBottomDialogFragment<String, DialogBottomSheetLocBinding>? =
        null

    override fun getLayoutId(): Int {
        return R.layout.activity_home
    }

    override fun setContainerLayout(): Int {
        return R.id.container
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        displayItNoStack(GetStartedFragment())

        //StatusBarUtil.setTransparent()


        viewDataBinding?.apply {

            imgNoti.setOnClickListener {
                displayItAddStack(NotificationFragment())
            }


            imgBookMark.setOnClickListener {
                displayItAddStack(MarkInterestFragment())
            }

            imgProfileAvatar.setOnClickListener {
                displayItAddStack(ProfileFragment())
            }

            imgBackBtn.setOnClickListener {
                popBack()
            }

            imgCrossbtn.setOnClickListener {
                popBack()
            }

            tvLoc.setOnClickListener {
                dialogSelectLoc()
            }

            tvNewChat.setOnClickListener {
                displayItAddStack(NewChatFragment())
            }

            imgCalender.setOnClickListener {
                displayItAddStack(SetupCalenderFragment())
            }

            imgGrpUser.setOnClickListener {
                displayItAddStack(AddmemberFragment())
            }

            tvCancel.setOnClickListener {
                popBack()
            }

            imgProfileSettings.setOnClickListener {
                displayItAddStack(SettingsFragment())
            }

            imgProfileCalender.setOnClickListener {
                displayItAddStack(CalenderFragment())
            }

        }

        bottomnavigtaionclick()
    }

    fun dialogSelectLoc() {
        bottomSheetDialog =
            ActionBottomDialogFragment<String, DialogBottomSheetLocBinding>(
                R.layout.dialog_bottom_sheet_loc, object :
                    BottomFragmentCallback<DialogBottomSheetLocBinding, String> {
                    override fun bindData(binder: DialogBottomSheetLocBinding) {

                        binder.apply {

                        }
                    }
                })

        bottomSheetDialog?.show(
            supportFragmentManager!!,
            ActionBottomDialogFragment::class.java.canonicalName
        )
    }

    fun setToolbarText(toolBarModel: ToolBarModel) {

        viewDataBinding?.apply {


            tcTitle.setText(toolBarModel.toolBarTtl ?: "")
            tvLeftTl.setText(toolBarModel.toolBarLetfTtl ?: "")

            if (toolBarModel.bottomNav) {
                bottomNavigationView.visibility = View.VISIBLE
            } else {
                bottomNavigationView.visibility = View.GONE
            }

            if (toolBarModel.locShow) {
                tvLoc.visibility = View.VISIBLE
            } else {
                tvLoc.visibility = View.GONE
            }

            if (toolBarModel.profileCalenderShow) {
                imgProfileCalender.visibility = View.VISIBLE
            } else {
                imgProfileCalender.visibility = View.GONE
            }

            if (toolBarModel.settingsShow) {
                imgProfileSettings.visibility = View.VISIBLE
            } else {
                imgProfileSettings.visibility = View.GONE
            }

            if (toolBarModel.newChatShow) {
                tvNewChat.visibility = View.VISIBLE
            } else {
                tvNewChat.visibility = View.GONE
            }


            if (toolBarModel.toolBarShow) {
                llToolbar.visibility = View.VISIBLE
            } else {
                llToolbar.visibility = View.GONE
            }


            if (toolBarModel.backBtn) {
                imgBackBtn.visibility = View.VISIBLE
            } else {
                imgBackBtn.visibility = View.GONE
            }

            if (toolBarModel.notiIconShow) {
                imgNoti.visibility = View.VISIBLE
            } else {
                imgNoti.visibility = View.GONE
            }

            if (toolBarModel.toolBarCancelShow) {
                tvCancel.visibility = View.VISIBLE
            } else {
                tvCancel.visibility = View.GONE
            }


            if (toolBarModel.toolBarProfileShow) {
                imgProfileAvatar.visibility = View.VISIBLE
            } else {
                imgProfileAvatar.visibility = View.GONE
            }
            if (toolBarModel.bookMarkShow) {
                imgBookMark.visibility = View.VISIBLE
            } else {
                imgBookMark.visibility = View.GONE
            }
            if (toolBarModel.leagueListShow) {
                llLeague.visibility = View.VISIBLE
            } else {
                llLeague.visibility = View.GONE
            }
            if (toolBarModel.chatShow) {
                llChat.visibility = View.VISIBLE
            } else {
                llChat.visibility = View.GONE
            }

            if (toolBarModel.imgGrpUserShow) {
                imgGrpUser.visibility = View.VISIBLE
            } else {
                imgGrpUser.visibility = View.GONE
            }

            if (toolBarModel.imgCalenderShow) {
                imgCalender.visibility = View.VISIBLE
            } else {
                imgCalender.visibility = View.GONE
            }

            if (toolBarModel.chatViewDetail) {
                tvViewDetail.visibility = View.VISIBLE
            } else {
                tvViewDetail.visibility = View.GONE
            }


            if (toolBarModel.toolBarleftTlShow) {
                tvLeftTl.visibility = View.VISIBLE
            } else {
                tvLeftTl.visibility = View.GONE
            }

            if (toolBarModel.toolBarCrossIconShow) {
                imgCrossbtn.visibility = View.VISIBLE
            } else {
                imgCrossbtn.visibility = View.GONE
            }

        }


    }

    fun bottomnavigtaionclick() {

        viewDataBinding?.bottomNavigationView?.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.action_activities -> {
                    displayItNoStack(ActivitiesFragment())
                    return@setOnNavigationItemSelectedListener true
                }

                R.id.action_player -> {
                    displayItNoStack(PlayersFragment())
                    return@setOnNavigationItemSelectedListener true
                }

                R.id.action_grp -> {
                    clearAllStack()
                    displayItNoStack(HomeFragment())
                    return@setOnNavigationItemSelectedListener true
                }

                R.id.action_chat -> {
                    displayItNoStack(ChatsFragment())
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.action_league -> {
                    displayItNoStack(LeaguesFragment())
                    return@setOnNavigationItemSelectedListener true
                }

                else -> {
                    return@setOnNavigationItemSelectedListener true
                }
            }

        }

    }

    /*//add
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragment = supportFragmentManager.findFragmentById(R.id.container)
        fragment!!.onActivityResult(requestCode, resultCode, data)

    }
*/
    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            //  var fragment = supportFragmentManager.findFragmentById(R.id.frameContainer)
            supportFragmentManager.popBackStack()
        } else {
            finishAffinity()

        }
    }

}