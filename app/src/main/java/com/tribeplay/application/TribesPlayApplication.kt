package com.tribeplay.application

import androidx.multidex.MultiDexApplication
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.PhoneAuthProvider

class TribesPlayApplication : MultiDexApplication() {
    companion object {
        @get:Synchronized
        lateinit var instance: TribesPlayApplication

        var resendToken: PhoneAuthProvider.ForceResendingToken? = null
    }

    override fun onCreate() {
        super.onCreate()
        instance = this


        FirebaseApp.initializeApp(this)
//        FirebaseApp.initializeApp(this)

    }
}