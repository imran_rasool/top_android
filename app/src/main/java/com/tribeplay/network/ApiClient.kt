package com.appsums.network

import android.util.Log
import androidx.annotation.NonNull
import com.tribeplay.application.TribesPlayApplication
import com.tribeplay.utils.GlobalConstants
import com.tribeplay.utils.SharedPrefClass
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit

object ApiClient {

    //   var BASE_URL = "http://3.109.89.215:3000/api/v1/"
    var BASE_URL = "http://api-dev.tribesofplay.com/"

    @JvmStatic
    private var mApiInterface: API? = null

    @JvmStatic
    fun getApiInterface(): API {
        return setApiInterface()
    }

    @JvmStatic
    private fun setApiInterface(): API {
        var mAuthToken = ""
        mAuthToken = SharedPrefClass().getPrefValue(
            TribesPlayApplication.instance, GlobalConstants.ACCESS_TOKEN
        ).toString()

        Log.d("mAuthToken", mAuthToken)


        val httpClient = OkHttpClient.Builder()
        httpClient
            .connectTimeout(5000, TimeUnit.MINUTES)
            .readTimeout(5000, TimeUnit.MINUTES)

        val mBuilder = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        httpClient.interceptors().add(logging)

        val interceptor: Interceptor = object : Interceptor {
            @Throws(IOException::class)
            override fun intercept(@NonNull chain: Interceptor.Chain): Response {
                val original = chain.request()
                val builder = original.newBuilder()

                if (mAuthToken.isNotEmpty()) {
                    val finalToken = GlobalConstants.BEARER + mAuthToken
                    builder.header("Authorization", finalToken)

                }
                val request = builder.build()
                val response = chain.proceed(request)
                return if (response.code == 401) {
                    response
                } else response
            }
        }

        if (!httpClient.interceptors().contains(interceptor)) {
            httpClient.addInterceptor(interceptor)
            mBuilder.client(httpClient.build())
            mApiInterface = mBuilder.build().create(API::class.java)
        }

        return mApiInterface!!
    }


}