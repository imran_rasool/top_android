package com.appsums.network

import com.tribeplay.model.*
import com.tribeplay.utils.GlobalConstants
import com.tribeplay.utils.SharedPrefClass
import com.tribeplay.view.activities.HomeActivity
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.io.File

object ApiCall {

    fun signupApi(
        homeActivity: HomeActivity, body: SignupModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .signUp(body)
            )
        }
    }

    fun verifyOTPApi(
        homeActivity: HomeActivity, body: OTPModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .verifyOtp(body)
            )
        }
    }

    fun uploadImgApi(
        homeActivity: HomeActivity,
        imageFile: String,
        userId: String,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            val hashMap = HashMap<String, RequestBody>()
            val userImage = File(imageFile)
            val body = userImage.asRequestBody("image/*".toMediaTypeOrNull())
            hashMap["user_image\"; filename=\"" + userImage.name] = body   //file==>user_image
            hashMap["user_id"] = userId.toRequestBody("text/plain".toMediaTypeOrNull())


            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .imgUploadApi(hashMap)
            )
        }
    }


    fun createProfileApi(
        homeActivity: HomeActivity,
        body: CreateProfileModel,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .createProfile(body)
            )
        }
    }

    fun loginApi(
        homeActivity: HomeActivity, body: LoginModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .login(body)
            )
        }
    }

    fun setLocationApi(
        homeActivity: HomeActivity, body: SetLocationModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .setLocation(body)
            )
        }
    }

    fun restPasswordApi(
        homeActivity: HomeActivity,
        body: ResetPasswordModel,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .resetPassword(body)
            )
        }
    }

    fun resendOTPApi(
        homeActivity: HomeActivity,
        body: ResendOTPModel,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .resendOTP(body)
            )
        }
    }

    fun sportCategoryApi(
        homeActivity: HomeActivity, callback: ResponseListener<GenericModel1>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel1>()
            mApiService.get(
                object : ApiResponse<GenericModel1> {
                    override fun onResponse(mResponse: Response<GenericModel1>) {
                        checkCode1(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel1>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .sportCategory()
            )
        }
    }

    fun selectCategoryApi(
        homeActivity: HomeActivity,
        body: SelectSportModel,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .selectSportCategory(body)
            )
        }
    }

    fun getSelectCategoryApi(
        homeActivity: HomeActivity,
        body: GetSelectSportModel,
        callback: ResponseListener<GenericModel2>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel2>()
            mApiService.get(
                object : ApiResponse<GenericModel2> {
                    override fun onResponse(mResponse: Response<GenericModel2>) {
                        checkCode2(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel2>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .getSelectSportCategory(body)
            )
        }
    }


    fun setPreferenceApi(
        homeActivity: HomeActivity,
        body: SetPreferenceModel,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .setPreference(body)
            )
        }
    }


    fun checkCode(
        mResponse: Response<GenericModel>,
        callback: ResponseListener<GenericModel>,
        homeActivity: HomeActivity
    ) {
        try {
            if (mResponse.body()?.status == true) {
                callback.onSuccess(mResponse)
            } else {
                homeActivity.showToast(mResponse.body()?.message)
                // homeActivity.logout()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun checkCode1(
        mResponse: Response<GenericModel1>,
        callback: ResponseListener<GenericModel1>,
        homeActivity: HomeActivity
    ) {
        if (mResponse.body()?.status == true) {
            callback.onSuccess(mResponse)
        } else {
            try {
                homeActivity.showToast(mResponse.body()?.message)
            } catch (e: Exception) {
                homeActivity.showToast(e.message)
            }
        }
    }

    fun checkCode2(
        mResponse: Response<GenericModel2>,
        callback: ResponseListener<GenericModel2>,
        homeActivity: HomeActivity
    ) {
        try {
            if (mResponse.body()?.status == true) {
                callback.onSuccess(mResponse)
            } else {
                homeActivity.showToast(mResponse.body()?.message)
                // homeActivity.logout()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


}

interface ResponseListener<T> {
    fun onSuccess(mResponse: Response<T>)
    fun onError(msg: String)
    fun onError(msg: Boolean) {
    }
}

