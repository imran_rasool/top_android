package com.appsums.network

import com.tribeplay.model.*
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface API {

    @POST("health/signup")
    fun signUp(
        @Body body: SignupModel
    ): Call<GenericModel>

    @POST("health/otp_verify")
    fun verifyOtp(
        @Body body: OTPModel
    ): Call<GenericModel>

    @Multipart
    @POST("health/upload_user_image")
    fun imgUploadApi(@PartMap hashMap: HashMap<String, RequestBody>): Call<GenericModel>

    @POST("health/create_profile")
    fun createProfile(
        @Body body: CreateProfileModel
    ): Call<GenericModel>

    @POST("health/login")
    fun login(
        @Body body: LoginModel
    ): Call<GenericModel>


    @POST("health/Set_location_radius")
    fun setLocation(
        @Body body: SetLocationModel
    ): Call<GenericModel>

    @POST("health/Reset_Password")
    fun resetPassword(
        @Body body: ResetPasswordModel
    ): Call<GenericModel>

    @POST("health/Resend_OTP")
    fun resendOTP(
        @Body body: ResendOTPModel
    ): Call<GenericModel>

    @GET("health/sport_category")
    fun sportCategory(
    ): Call<GenericModel1>

    @POST("health/Select_Sport_category")
    fun selectSportCategory(
        @Body body: SelectSportModel
    ): Call<GenericModel>

    @POST("health/Selected_Sport_category")
    fun getSelectSportCategory(
        @Body body: GetSelectSportModel
    ): Call<GenericModel2>

    @POST("health/Set_preferences")
    fun setPreference(
        @Body body: SetPreferenceModel
    ): Call<GenericModel>


}