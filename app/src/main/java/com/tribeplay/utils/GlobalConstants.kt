package com.tribeplay.utils

import com.tribeplay.R
import com.tribeplay.application.TribesPlayApplication

object GlobalConstants {

    @JvmStatic
    val BASE_URL = "http://3.19.69.119/api/"

    @JvmStatic
    val BASE_URL_IMG = "http://3.19.69.119/public/"

    @JvmStatic
    val SHARED_PREF = TribesPlayApplication.instance.resources.getString(R.string.shared_pref)

    @JvmStatic
    val ACCESS_TOKEN = "access_token"

    @JvmStatic
    var BEARER = "Bearer "

    @JvmStatic
    var DEVICE_ID = "device_id"

    var UNIQUE_DEVICE_ID = "unique_device_id"

    @JvmStatic
    var Device_Type = "A"

    @JvmStatic
    var APP_VERSION = "1.0"

    @JvmStatic
    val USERID = "user_id"

    @JvmStatic
    val EMAIL = "EMAIL"

    @JvmStatic
    val FIRST_NAME = "firstName"

    @JvmStatic
    val LAST_NAME = "lastName"

    @JvmStatic
    val PREFERENCE = "preference"

    val requestKey = "requestKey"

    @JvmStatic
    val FILTER_OBJECT = "FILTER_OBJECT"


    const val USERID1 = "userId1"

    //Google AUTO PLACE Search API
    const val API_KEY = "AIzaSyDfOO5Uf15uxOc1sjYlgx5jRtOCkrw01yo"


    const val EMAIL_PATTERN =
        "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
}




